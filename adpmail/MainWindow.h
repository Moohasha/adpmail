#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtGui/QSystemTrayIcon>
#include "Client.h"

namespace Ui {
  class StatusWidget;
  class MainWindow;
}
namespace msgs {struct MAIL;}
namespace msgs {struct CLIENTS;}
class QTableWidgetItem;
class QSystemTrayIcon;
class QListWidgetItem;

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  
  void init(void);

public slots:
  void statusMessage(QString msg);
  void showErrorMessage(QString msg);

private slots:
  void showAbout();
  void proceedWithStartup();
  void wizardLogin();
  void connectClicked();
  void loginClicked();
  void changePasswordClicked();
  void newMailClicked();
  void replyToOrForwardClicked();
  void deleteClicked();
  void updateConnectionStatus(int status);
  void sendMail(msgs::MAIL* mail);
  void receivedMail(unsigned int uid);
  void mailRowClicked(int row);
  void passwordChangeComplete();
  void receivedClients(msgs::CLIENTS* msg);
  void trayIconClicked(QSystemTrayIcon::ActivationReason);
  void trayMessageClicked();
  void attachmentClicked(QListWidgetItem*);

private:
  void closeEvent(QCloseEvent*);
  void restoreSettings();
  bool initDB();
  bool restoreHost();
  void attemptLogin();
  void clientConnected();
  void deleteMail(QList<unsigned int>& uids);
  unsigned int getSelectedMail() const;
  void setMailRead(msgs::MAIL& mail, bool read);
  void setRowRead(int row, bool read);
  int getMailRow(unsigned int uid);

private:
  Ui::MainWindow *ui;
  Ui::StatusWidget* mHostStatus;
  Ui::StatusWidget* mLoginStatus;
  ConnectionStatus mConnectionStatus;
  Client mClient;
  QSystemTrayIcon* mTrayIcon;
  QString mCurrentEmail;
  QString mCurrentPassword;
  bool mClientDB;
  QMap<QString, QString> mUsers;
  QMap<unsigned int, QTableWidgetItem*> mMailItems; // key = mail uid, value = mail item
  unsigned int mLastMailRecvd;
};

#endif // MAINWINDOW_H
