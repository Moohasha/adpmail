#include <QtCore/QSettings>
#include <QtCore/QDebug>
#include <QtCore/QDir>
#include <QtSql/QtSql>
#include <QtGui/QMessageBox>
#include <QtGui/QFileIconProvider>
#include <QtGui/QDesktopServices>
#include "MainWindow.h"
#include "ComposeDialog.h"
#include "NewClientWizard.h"
#include "ClientDataBaseHelper.h"
#include "MailMeta.h"
#include "../common.h"
#include "ui_MainWindow.h"
#include "ui_LoginDialog.h"
#include "ui_ChangePasswordDialog.h"
#include "ui_NewUserDialog.h"
#include "ui_ConnectDialog.h"
#include "ui_StatusWidget.h"

static const QString CLIENT_DB_NAME("Client.sqlite");
#define CREATE_SETTINGS(var_name) QSettings var_name("Client.settings", QSettings::IniFormat);

enum INBOX_COLUMNS
{
  COL_STATUS = 0,
  COL_ATTACHMENT = 1,
  COL_FROM = 2,
  COL_SUBJECT = 3,
  COL_DATE = 4
};

// ----------------------------------------------------------------------------
// ctor
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  mHostStatus(new Ui::StatusWidget),
  mLoginStatus(new Ui::StatusWidget),
  mConnectionStatus(NotConnected),
  mTrayIcon(new QSystemTrayIcon(QIcon(":/images/mail_unread"), this)),
  mClientDB(false),
  mLastMailRecvd(0)
{
  // Setup UI
  ui->setupUi(this);
  ui->foldersTree->hide(); // TEMP: Until handling different folders, just hide this

  QString title = QString("%1 %2.%3")
      .arg(windowTitle())
      .arg(CLIENT_VERSION_MAJOR)
      .arg(CLIENT_VERSION_MINOR);
  setWindowTitle(title);

  // Status widget for host connection
  QWidget* widget1 = new QWidget;
  mHostStatus->setupUi(widget1);
  mHostStatus->light->setPixmap(QPixmap(":/images/status_red"));
  mHostStatus->status->setText("Connection to host:");
  statusBar()->addPermanentWidget(widget1);

  // Status widget for login
  QWidget* widget2 = new QWidget;
  mLoginStatus->setupUi(widget2);
  mLoginStatus->light->setPixmap(QPixmap(":/images/status_red"));
  mLoginStatus->status->setText("Logged in:");
  statusBar()->addPermanentWidget(widget2);

  // Connect signals from the tray icon
  connect(mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(trayIconClicked(QSystemTrayIcon::ActivationReason)));
  connect(mTrayIcon, SIGNAL(messageClicked()), this, SLOT(trayMessageClicked()));
}

// ----------------------------------------------------------------------------
// dtor
// ----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
  delete mTrayIcon;
  delete mHostStatus;
  delete mLoginStatus;
  delete ui;
}

// ----------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------
void MainWindow::init()
{
  // Restore settings
  restoreSettings();

  // Make sure the attachments path exists
  QDir dir;
  if (!dir.exists(ATTACHMENTS_PATH)) {
    dir.mkpath(ATTACHMENTS_PATH);
  }

  // Connect to the signals from the client.  Do this before it starts so that
  // we can immediately start getting signals.
  connect(&mClient, SIGNAL(connectionStatusChanged(int)), this, SLOT(updateConnectionStatus(int)));
  connect(&mClient, SIGNAL(errorMessage(QString)), this, SLOT(showErrorMessage(QString)));
  connect(&mClient, SIGNAL(receivedMail(unsigned int)), this, SLOT(receivedMail(unsigned int)));
  connect(&mClient, SIGNAL(passwordChangeComplete()), this, SLOT(passwordChangeComplete()));
  connect(&mClient, SIGNAL(receivedClients(msgs::CLIENTS*)), this, SLOT(receivedClients(msgs::CLIENTS*)));

  // Start the client so that it's running when the host address is read in and
  // passed to it.
  mClient.start();
  mClient.moveToThread(&mClient);

  QFile file("client.settings");
  if (file.exists())
  {
    proceedWithStartup();
  }
  else
  {
    hide();

    // Create new client wizard
    NewClientWizard* wizard = new NewClientWizard(mClient, this);
    wizard->setAttribute(Qt::WA_DeleteOnClose);
    wizard->setModal(true);

    //connect(wizard, SIGNAL(createUser()), this, SLOT(wizardCreateUser()));
    //connect(wizard, SIGNAL(login()), this, SLOT(wizardLogin()));
    connect(wizard, SIGNAL(finished(int)), this, SLOT(proceedWithStartup()));
    wizard->show();
  }
}

// ----------------------------------------------------------------------------
// statusMessage
// ----------------------------------------------------------------------------
void MainWindow::statusMessage(QString msg)
{
  statusBar()->showMessage(msg, 10000);
}

// ----------------------------------------------------------------------------
// showErrorMessage
// ----------------------------------------------------------------------------
void MainWindow::showErrorMessage(QString msg)
{
  QMessageBox::critical(this, "Error", msg);
  statusMessage(msg);
}

// ----------------------------------------------------------------------------
// showAbout
// ----------------------------------------------------------------------------
void MainWindow::showAbout()
{
  QMessageBox::about(this, "ADP Mail",
    "Because Lockheed is cheap and their FOSS policy sucks.\n\n"
    "Version 1.0");
}

// ----------------------------------------------------------------------------
// proceedWithStartup
// ----------------------------------------------------------------------------
void MainWindow::proceedWithStartup()
{
  // Try to reload host info
  if (restoreHost())
  {
    // Prompt user for email and password withi which to login
    loginClicked();
  }

  show();
  mTrayIcon->show();
}

// ----------------------------------------------------------------------------
// wizardLogin
// ----------------------------------------------------------------------------
void MainWindow::wizardLogin()
{
  qDebug() << "TEMP: wizardLogin";
  // TODO: Try to connect to host
  // TODO: Send LOGIN message
}

// ----------------------------------------------------------------------------
// connectClicked
// ----------------------------------------------------------------------------
void MainWindow::connectClicked()
{
  QDialog dialog(this);
  Ui::ConnectDialog ui_d;
  ui_d.setupUi(&dialog);
  ui_d.host->setText(mClient.getHost());
  if (dialog.exec() == QDialog::Accepted)
  {
    QString hostName = ui_d.host->text();

    // Write to settings
    CREATE_SETTINGS(settings);
    settings.setValue("Host", hostName);

    // Connect to the host
    mClient.connect(hostName);
  }
}

// ----------------------------------------------------------------------------
// loginClicked
// ----------------------------------------------------------------------------
void MainWindow::loginClicked()
{
  // Get last user name from settings
  CREATE_SETTINGS(settings);
  QString email = settings.value("User").toString();
  QString password;

  // Create dialog to prompt user for email and password
  QDialog dialog(this);
  Ui::LoginDialog ui_d;
  ui_d.setupUi(&dialog);
  ui_d.email->setText(email);
  if (!email.isEmpty()) {
    ui_d.password->setFocus();
  }
  if (dialog.exec() == QDialog::Accepted)
  {
    email = ui_d.email->text();
    password = ui_d.password->text();

    // Attempt to login to the server
    if (!email.isEmpty() && !password.isEmpty())
    {
      settings.setValue("User", email);
      mCurrentEmail = email;
      mCurrentPassword = password;
      attemptLogin();
    }
  }
}

// ----------------------------------------------------------------------------
// changePasswordClicked
// ----------------------------------------------------------------------------
void MainWindow::changePasswordClicked()
{
  QDialog dialog(this);
  Ui::ChangePasswordDialog ui_d;
  ui_d.setupUi(&dialog);
  if (dialog.exec() == QDialog::Accepted)
  {
    if (ui_d.newPassword1->text() == ui_d.newPassword2->text())
    {
      msgs::CHG_PWD msg;
      msg.old_pwd = ui_d.oldPassword->text().toAscii();
      msg.new_pwd = ui_d.newPassword1->text().toAscii();
      mClient.sendChangePassword(&msg);
    }
    else
    {
      QMessageBox::critical(this, "Change Password", "New passwords do not match.  You might not want to go with that password\n"
        "if you can't type it two times in a row...");
    }
  }
}

// ----------------------------------------------------------------------------
// newMailClicked
// ----------------------------------------------------------------------------
void MainWindow::newMailClicked()
{
  ComposeDialog* dialog = new ComposeDialog(mCurrentEmail);
  dialog->setUserList(mUsers);
  connect (dialog, SIGNAL(sendMail(msgs::MAIL*)), this, SLOT(sendMail(msgs::MAIL*)));
  dialog->show();
}

// ----------------------------------------------------------------------------
// replyToOrForwardClicked
// ----------------------------------------------------------------------------
void MainWindow::replyToOrForwardClicked()
{
  unsigned int uid = getSelectedMail();
  if (uid > 0)
  {
    msgs::MAIL mail;
    QString err;
    if (cdbh::getMail(uid, mail, err))
    {
      ComposeDialog* dialog = new ComposeDialog(mCurrentEmail);
      dialog->setUserList(mUsers);
      if (sender() == ui->action_Forward) {
        dialog->init(mail, ComposeDialog::Forward);
      } else if (sender() == ui->actionReply_to_All) {
        dialog->init(mail, ComposeDialog::ReplyToAll);
      } else {
        dialog->init(mail, ComposeDialog::ReplyTo);
      }
      connect (dialog, SIGNAL(sendMail(msgs::MAIL*)), this, SLOT(sendMail(msgs::MAIL*)));
      dialog->show();
    }
    else
    {
      QMessageBox::critical(this, "Reply to", "Could not locate mail to edit: " + err);
    }
  }
}

// ----------------------------------------------------------------------------
// deleteClicked
// ----------------------------------------------------------------------------
void MainWindow::deleteClicked()
{
  // Delete Email
  msgs::MAIL_DEL msg;

  // Get all selected rows in the table
  QList<QTableWidgetSelectionRange> ranges = ui->mailTable->selectedRanges();
  foreach (const QTableWidgetSelectionRange& range, ranges)
  {
    qDebug() << "Deleting rows " << range.topRow() << " through " << range.bottomRow();
    for (int row = range.topRow(); row <= range.bottomRow(); ++row)
    {
      unsigned int uid = ui->mailTable->item(row, 0)->data(Qt::UserRole).toUInt();
      msg.uids << uid;
    }
  }

  // Send the MAIL_DEL message to the server
  mClient.sendOther(&msg);

  // Delete all selected mail
  deleteMail(msg.uids);
}

// ----------------------------------------------------------------------------
// closeEvent
// ----------------------------------------------------------------------------
void MainWindow::closeEvent(QCloseEvent*)
{
  // Save current settings
  CREATE_SETTINGS(settings);
  settings.setValue("Geometry", saveGeometry());
  settings.setValue("WindowState", saveState());
  settings.setValue("SplitterV", ui->vertSplitter->saveState());
  settings.setValue("SplitterH", ui->horzSplitter->saveState());
  settings.setValue("MailHeader", ui->mailTable->horizontalHeader()->saveState());
}

// ----------------------------------------------------------------------------
// restoreSettings
// ----------------------------------------------------------------------------
void MainWindow::restoreSettings()
{
  CREATE_SETTINGS(settings);

  // Geometry
  QByteArray geo(settings.value("Geometry").toByteArray());
  if (!geo.isNull()) {
    restoreGeometry(geo);
  }

  // Window State
  QByteArray ws(settings.value("WindowState").toByteArray());
  if (!ws.isNull()) {
    restoreState(ws);
  }

  // Vertical Splitter
  QByteArray vs(settings.value("SplitterV").toByteArray());
  if (!vs.isNull()) {
    ui->vertSplitter->restoreState(vs);
  }

  // Horizontal Splitter
  QByteArray hs(settings.value("SplitterH").toByteArray());
  if (!hs.isNull()) {
    ui->horzSplitter->restoreState(hs);
  }

  // Mail Header
  QByteArray header(settings.value("MailHeader").toByteArray());
  if (!header.isNull()) {
    ui->mailTable->horizontalHeader()->restoreState(header);
  }

  // Reset the first column to 25 pixels and make it a fixed size
  ui->mailTable->setColumnWidth(COL_STATUS, 25);
  ui->mailTable->setColumnWidth(COL_ATTACHMENT, 25);
  ui->mailTable->horizontalHeader()->setResizeMode(COL_STATUS, QHeaderView::Fixed);
  ui->mailTable->horizontalHeader()->setResizeMode(COL_ATTACHMENT, QHeaderView::Fixed);
}

// ----------------------------------------------------------------------------
// initDB
// ----------------------------------------------------------------------------
bool MainWindow::initDB()
{
  bool dbExists = false;

  // User the user's email address as the database name
  QString dbPath = mCurrentEmail;
  if (QFile::exists(dbPath)) {
    dbExists = true;
  }

  // Create the database connection and connect to it
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName(dbPath);
  if (db.open())
  {
    qDebug() << "Client database connected";

    // If there's an init file, run it.
    QFile file("init_client_db.sql");
    if (file.open(QFile::ReadOnly))
    {
      QString scriptFile(file.readAll());
      QStringList scripts(scriptFile.split(";", QString::SkipEmptyParts));
      foreach (QString script, scripts)
      {
        // Make sure the "script" isn't just whitespace
        script = script.trimmed();
        if (!script.isEmpty())
        {
          QSqlQuery query;
          if (!query.exec(script)) {
            QMessageBox::critical(this, "Client Database Error", query.lastError().text());
          }
        }
      }
    }
    mClientDB = true;
  }
  else
  {
    QMessageBox::critical(this, "Client Database Error", 
      "Could not connect to client database!  Will not be able to receive mail.");
  }

  return dbExists;
}

// ----------------------------------------------------------------------------
// restoreHost
// ----------------------------------------------------------------------------
bool MainWindow::restoreHost()
{
  CREATE_SETTINGS(settings);
  QString host(settings.value("Host").toString());
  if (!host.isEmpty())
  {
    mClient.connect(host);
  }
  else
  {
    // Prompt user for host name/address
    connectClicked();

    // Try again after prompting the user
    host = settings.value("Host").toString();
  }
  return !host.isEmpty();
}

// ----------------------------------------------------------------------------
// attemptLogin
// ----------------------------------------------------------------------------
void MainWindow::attemptLogin()
{
  // Make sure we currently have an email and password to send, and that we're
  // connected to the server.
  if (!mCurrentEmail.isEmpty()
   && !mCurrentPassword.isEmpty()
   && mConnectionStatus >= Connected)
  {
    // Send message to server to connect using these credentials
    msgs::LOGIN msg;
    msg.email = mCurrentEmail;
    msg.password = mCurrentPassword.toLatin1();
    mClient.sendLogin(&msg);
    statusBar()->showMessage("Logged in", 5000);
  }
  else
  {
    if (mConnectionStatus == NotConnected) {
      statusBar()->showMessage("Login failed: Not connected");
    } else if (mCurrentEmail.isEmpty()) {
      statusBar()->showMessage("Login failed: User email not specified.");
    } else if (mCurrentPassword.isEmpty()) {
      statusBar()->showMessage("Login failed: User password not specified.");
    }
  }
}

// ----------------------------------------------------------------------------
// updatemConnectionStatus
// ----------------------------------------------------------------------------
void MainWindow::updateConnectionStatus(int status)
{
  mConnectionStatus = static_cast<ConnectionStatus>(status);
  mHostStatus->light->setPixmap(QPixmap(":/images/status_red"));
  mLoginStatus->light->setPixmap(QPixmap(":/images/status_red"));

  switch (mConnectionStatus)
  {
  case LoggedIn:
    mLoginStatus->light->setPixmap(QPixmap(":/images/status_green"));
    mLoginStatus->status->setText("Logged in as: " + mCurrentEmail);
    clientConnected();
    // flow through

  case Connected:
    mHostStatus->light->setPixmap(QPixmap(":/images/status_green"));

    // If not logged in, try to log in again.
    if (mConnectionStatus != LoggedIn) {
      attemptLogin();
    }
    break;
  }
}

// ----------------------------------------------------------------------------
// sendMail
// ----------------------------------------------------------------------------
void MainWindow::sendMail(msgs::MAIL* mail)
{
  mClient.sendMail(mail);
  delete mail;
}

// ----------------------------------------------------------------------------
// receivedMail
// ----------------------------------------------------------------------------
void MainWindow::receivedMail(unsigned int uid)
{
  MailMeta meta;
  QString err;
  if (cdbh::getMailMeta(uid, meta, err))
  {
    ui->mailTable->setSortingEnabled(false); // disable sorting
    ui->mailTable->insertRow(0);
    ui->mailTable->setItem(0, COL_STATUS, new QTableWidgetItem(QString()));
    if (meta.has_attachment) {
      ui->mailTable->setItem(0, COL_ATTACHMENT, new QTableWidgetItem(QIcon(":/images/attachment"), QString()));
    } else {
      ui->mailTable->setItem(0, COL_ATTACHMENT, new QTableWidgetItem(QString()));
    }
    ui->mailTable->setItem(0, COL_FROM, new QTableWidgetItem(meta.from));
    ui->mailTable->setItem(0, COL_SUBJECT, new QTableWidgetItem(meta.subject));
    ui->mailTable->setItem(0, COL_DATE, new QTableWidgetItem(meta.sent_time.toString()));

    // Associate the uid with the first item
    ui->mailTable->item(0, 0)->setData(Qt::UserRole, uid);

    // Save the table item with this mail's uid
    mMailItems.insert(uid, ui->mailTable->item(0, 0));
    mLastMailRecvd = uid;

    // Update the row to reflect whether or not this mail is read
    setRowRead(0, meta.has_read);

    // reenable sorting
    ui->mailTable->setSortingEnabled(true);

    // Show message in system tray about new mail received
    QString title = "New Mail";
    QString msg = QString("From: %1\nSubject: %2").arg(meta.from).arg(meta.subject);
    mTrayIcon->showMessage(title, msg, QSystemTrayIcon::NoIcon);
  }
}

// ----------------------------------------------------------------------------
// receivedMail
// ----------------------------------------------------------------------------
void MainWindow::mailRowClicked(int row)
{
  ui->previewPane->clear();

  // Get the Mail uid from the item
  unsigned int uid = getSelectedMail();
  if (uid > 0)
  {
    // Get the Mail from the database
    msgs::MAIL mail;
    QString err;
    if (cdbh::getMail(uid, mail, err))
    {
      // Make sure this Mail is flagged as read by this user
      setMailRead(mail, true);

      // Show the mail in the preview pane
      ui->previewPane->setHtml(mail.body);

      // Check attachments
      ui->attachmentList->clear();
      ui->attachmentList->setVisible(mail.attachments.count() > 0);
      QMap<QString, QByteArray>::const_iterator citr = mail.attachments.begin();
      for (; citr != mail.attachments.end(); ++citr)
      {
        QString filename = citr.key();
        QString uuid = citr.value();

        // Need to get the icon for this file type.  Only way I know to do that
        // is to actually create a file with this type somewhere and use the
        // QFileIconProvider to get the icon.  So just create a temporary file
        // with this file's extension, grab the icon, and then remove it.
        // TODO: Don't hardcode this.  Lookup the environment variable.
        static const QString TEMP_PATH("C:/Temp/");
        QString tempName = TEMP_PATH + QUuid::createUuid().toString() + filename;
        QFile file(tempName);
        if (file.open(QFile::WriteOnly)) // creates the file
        {
          file.close();
        }
        QFileInfo fi(tempName);
        QIcon icon = QFileIconProvider().icon(fi); // get the icon
        file.remove(); // remove the temp file now that we're done with it.

        // Add attachment item, store the uuid with it
        QListWidgetItem* item = new QListWidgetItem(icon, filename);
        item->setData(Qt::UserRole, uuid);
        ui->attachmentList->addItem(item);
      }
    }
    else
    {
      statusBar()->showMessage(err, 10000);
    }
  }
}

// ----------------------------------------------------------------------------
// passwordChangeComplete
// ----------------------------------------------------------------------------
void MainWindow::passwordChangeComplete()
{
  QMessageBox::information(this, "Change Password", "Password successfully changed.");
}

// ----------------------------------------------------------------------------
// passwordChangeComplete
// ----------------------------------------------------------------------------
void MainWindow::receivedClients(msgs::CLIENTS* msg)
{
  if (msg) {
    mUsers = msg->names;
  }
}

// ----------------------------------------------------------------------------
// trayIconClicked
// ----------------------------------------------------------------------------
void MainWindow::trayIconClicked(QSystemTrayIcon::ActivationReason reason)
{
  if (reason == QSystemTrayIcon::DoubleClick)
  {
    // Show and raise the main window
    showNormal();
    raise();
    activateWindow();
  }
}

// ----------------------------------------------------------------------------
// attachmentClicked
// ----------------------------------------------------------------------------
void MainWindow::attachmentClicked(QListWidgetItem* item)
{
  // Get the file uuid from the attachment
  QString uuid = item->data(Qt::UserRole).toString();
  QString attPath = ATTACHMENTS_PATH + uuid;

  QFile attFile(attPath);
  if (attFile.open(QFile::ReadOnly))
  {
    // Copy the file to the temp folder, give it a unique name
    QTemporaryFile tempFile(QDir::tempPath() + "/XXXXXX-" + item->text());
    if (tempFile.open())
    {
      tempFile.setAutoRemove(false); // don't auto remove
      tempFile.write(attFile.readAll());
      tempFile.setPermissions((QFile::Permissions)0x5555); // Everyone can read/exec, but not write
      tempFile.close();
      if (!QDesktopServices::openUrl(QUrl("file:///" + tempFile.fileName(), QUrl::TolerantMode)))
      {
        showErrorMessage("Could not open attachment.");
      }
    }
  }
  else
  {
    showErrorMessage("Could not find attachment file.");
  }
}

// ----------------------------------------------------------------------------
// trayMessageClicked
// ----------------------------------------------------------------------------
void MainWindow::trayMessageClicked()
{
  // Show and raise the main window
  trayIconClicked(QSystemTrayIcon::DoubleClick);

  // Select the most recent mail row.
  if (QTableWidgetItem* item = mMailItems.value(mLastMailRecvd, 0)) {
    ui->mailTable->selectRow(ui->mailTable->row(item));
  }
}

// ----------------------------------------------------------------------------
// clientConnected
// ----------------------------------------------------------------------------
void MainWindow::clientConnected()
{
  ui->mailTable->clearContents();
  ui->mailTable->setRowCount(0);
  ui->previewPane->clear();

  // Make sure the client database is created and connected to
  initDB();

  // Query for all cached email
  QList<unsigned int> uids;
  QString err;
  if (cdbh::getAllCachedMailUids(uids, err))
  {
    // Send message to server to request any missing Mail
    msgs::MAIL_REQ msg;
    msg.uids = uids;
    mClient.sendOther(&msg);

    // Call receivedMail for each cached uid to add it to the mail table
    foreach (unsigned int uid, uids) {
      receivedMail(uid);
    }
  }
}

// ----------------------------------------------------------------------------
// deleteMail
// ----------------------------------------------------------------------------
void MainWindow::deleteMail(QList<unsigned int>& uids)
{
  foreach (unsigned int uid, uids)
  {
    // Remove the mail from the database
    QString err;
    cdbh::deleteMail(uid, err);

    // Remove the mail from the table
    if (QTableWidgetItem* item = mMailItems.take(uid))
    {
      int row = ui->mailTable->row(item);
      ui->mailTable->removeRow(row);

      // Hide attachment list incase it was shown
      ui->attachmentList->hide();
    }
  }
}

// ----------------------------------------------------------------------------
// deleteMail
// ----------------------------------------------------------------------------
unsigned int MainWindow::getSelectedMail() const
{
  unsigned int uid = 0;

  // Get the current row
  int row = ui->mailTable->currentRow();

  // Get the first item in the row
  if (QTableWidgetItem* item = ui->mailTable->item(row, 0))
  {
    // Get the UID from the item
    bool ok = false;
    unsigned int temp = item->data(Qt::UserRole).toUInt(&ok);
    if (ok) {
      uid = temp;
    }
  }
  return uid;
}

// ----------------------------------------------------------------------------
// setMailRead
// ----------------------------------------------------------------------------
void MainWindow::setMailRead(msgs::MAIL& mail, bool read)
{
  // If the mail hasn't been read, flag it as read
  if (mail.has_read != read)
  {
    mail.has_read = read;

    // Update client database
    QString err;
    if (cdbh::setMailRead(mail.uid, err))
    {
      // Notify server
      msgs::MAIL_UPDATE msg;
      msg.uid = mail.uid;
      msg.update_type = msgs::MAIL_UPDATE::Read;
      msg.has_read = true;
      mClient.sendOther(&msg);
    }

    // Update table
    int row = getMailRow(mail.uid);
    if (row >= 0) {
      setRowRead(row, mail.has_read);
    }
  }
}

// ----------------------------------------------------------------------------
// setRowRead
// ----------------------------------------------------------------------------
void MainWindow::setRowRead(int row, bool read)
{
  if (row >= 0)
  {
    QIcon icon = (read ? QIcon(":/images/mail_read") : QIcon(":/images/mail_unread"));
    QFont f = font();
    f.setBold(!read); // make the font bold if it hasn't been read

    // Update the items in the row
    ui->mailTable->item(row, 0)->setIcon(icon);
    for (int col = 1; col < ui->mailTable->columnCount(); ++ col) {
      ui->mailTable->item(row, col)->setFont(f);
    }
  }

  // Update the notification system try icon
  unsigned int count = 0;
  QString err;
  cdbh::getNumUnreadMail(count, err);
  if (count > 0) {
    mTrayIcon->show(); // show icon
  } else {
    mTrayIcon->hide(); // hide icon
  }

}

// ----------------------------------------------------------------------------
// getMailRow
// ----------------------------------------------------------------------------
int MainWindow::getMailRow(unsigned int uid)
{
  int row = -1;

  for (int i = 0; i < ui->mailTable->rowCount(); ++i)
  {
    unsigned int itemUid = ui->mailTable->item(i, 0)->data(Qt::UserRole).toUInt();
    if (uid == itemUid) {
      row = i;
      break;
    }
  }

  return row;
}
