
CREATE TABLE IF NOT EXISTS Mail
(
  uid INT NOT NULL,
  category INT NOT NULL,
  has_read BOOLEAN NOT NULL,
  has_att BOOLEAN NOT NULL,
  to_emails TEXT NOT NULL,
  cc_emails TEXT,
  from_email CHAR(128) NOT NULL,
  subject CHAR(128) NOT NULL,
  sent_time DATETIME NOT NULL,
  body TEXT,
  PRIMARY KEY (uid)
);
