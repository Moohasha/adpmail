#include <math.h>
#include <QtCore/QFileInfo>
#include <QtGui/QFileIconProvider>
#include "AttachmentWidget.h"
#include "ui_AttachmentWidget.h"

AttachmentWidget::AttachmentWidget(const QString& path, QWidget *parent)
  : QFrame(parent),
    ui(new Ui::AttachmentWidget),
    mPath(path)
{
  static QFileIconProvider iconProvider;
  QFileInfo fi(path);
  ui->setupUi(this);

  setFrameStyle(QFrame::Box | QFrame::Plain);

  // Calculate the file size in KB or MB
  int size = 0;
  QChar suffix = 'K';

  static int TEN_MB = (1024*1024*10); // 10 MB
  if (fi.size() < TEN_MB)
  {
    size = ceil(fi.size()/1024.0); // kilobytes
    suffix = 'K';
  }
  else
  {
    size = ceil(fi.size()/1048576.0); // megabytes
    suffix = 'M';
  }

  // Update UI
  QString sizeStr = QString("(%1%2)")
      .arg(size)
      .arg(suffix);
  ui->lblIcon->setPixmap(iconProvider.icon(fi).pixmap(16, 16));
  ui->lblFilename->setText(fi.fileName());
  ui->lblSize->setText(sizeStr);

  // Connect the remove button's clicked signal to the removeClicked signal
  connect (ui->btnRemove, SIGNAL(clicked()), this, SIGNAL(removeClicked()));

  // Load the file
  // TODO: Make this threaded if loading large files becomes combersome.
  QFile file(path);
  if (file.open(QFile::ReadOnly)) {
    mData = file.readAll();
  }
}

AttachmentWidget::~AttachmentWidget()
{
  delete ui;
}

QString AttachmentWidget::getFilename() const
{
  return QFileInfo(mPath).fileName();
}

QString AttachmentWidget::getPath() const
{
  return mPath;
}

QByteArray AttachmentWidget::getFileData() const
{
  return mData;
}
