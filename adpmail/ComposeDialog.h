#ifndef COMPOSEDIALOG_H
#define COMPOSEDIALOG_H
#include <QDialog>
#include <QtCore/QMap>

namespace Ui {class ComposeDialog;}
namespace msgs {struct MAIL;}
class AttachmentWidget;

class ComposeDialog : public QDialog
{
  Q_OBJECT

signals:
  void sendMail(msgs::MAIL*);

public:
  enum Mode
  {
    ReplyTo,
    ReplyToAll,
    Forward
  };

  ComposeDialog(QString userEmail, QWidget* parent = 0);
  ~ComposeDialog(void);

  void init(const msgs::MAIL& mail, Mode mode);
  void setUserList(const QMap<QString, QString>& users);

private slots:
  void toClicked();
  void ccClicked();
  void accept();
  void addAttachmentClicked();
  void removeAttachmentClicked();

private:
  void initEditor();
  QStringList parseUserNames(const QString& str, bool validate);
  void addAttachment(const QString& path);

private:
  Ui::ComposeDialog* ui;
  QString userEmail;
  QMap<QString, QString> mUsers;
  QMap<QString, AttachmentWidget*> mAttachments; // key = path
};

#endif // COMPOSEDIALOG_H
