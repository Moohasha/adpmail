#ifndef CLIENTDATABASEHELPER_H
#define CLIENTDATABASEHELPER_H
#include "../common.h"

struct MailMeta;

// Client Database Helper (cdbh) methods
namespace cdbh
{
  // Save the Mail message to the database.
  // Note: The uid of the Mail message may change based on the result of 
  // inserting it into the database.
  bool saveMail(msgs::MAIL& mail, QString& err);

  // Retrieve the meta data for a specified Mail message.
  bool getMailMeta(unsigned int uid, MailMeta& meta, QString& err);

  // Retrieve an entire Mail message.
  bool getMail(unsigned int uid, msgs::MAIL& mail, QString& err);

  // Returns a list of all cached Mail uids.
  bool getAllCachedMailUids(QList<unsigned int>& uids, QString& err);

  // Delete the specified Mail from the database
  bool deleteMail(unsigned int uid, QString& err);

  // Flag the specified Mail as being read
  bool setMailRead(unsigned int uid, QString& err);

  // Returns the number of unread mail messages
  bool getNumUnreadMail(unsigned int& count, QString& err);
} // end namespace

#endif // CLIENTDATABASEHELPER_H
