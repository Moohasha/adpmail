#include <QtCore/QDebug>
#include "NewClientWizard.h"
#include "Client.h"
#include "ui_NewClientWizard.h"

NewClientWizard::NewClientWizard(Client& client, QWidget *parent) :
  QWizard(parent),
  client(client),
  ui(new Ui::NewClientWizard),
  pageValid(true),
  finishing(false)
{
  ui->setupUi(this);

  //// Hide all error messages by default
  //ui->createEmailError->hide();
  //ui->createPasswordError1->hide();
  //ui->createPasswordError2->hide();
  //ui->hostError->hide();
  //ui->loginEmailError->hide();
  //ui->loginPasswordError->hide();
  //ui->nameError->hide();

  connect(&client, SIGNAL(errorMessage(QString)), this, SLOT(clientError(QString)));
  connect(&client, SIGNAL(connectionStatusChanged(int)), this, SLOT(clientStatusChanged(int)));
}

NewClientWizard::~NewClientWizard()
{
  delete ui;
}

void NewClientWizard::next()
{
  //pageValid = true;
  //switch (currentId())
  //{
  //case ServerPage:
  //  pageValid = confirmServerPage();
  //  break;

  //case LoginPage:
  //  pageValid = confirmLoginPage();
  //  break;

  //default:
  //  pageValid = true;
  //}

  QWizard::next();
}

int NewClientWizard::nextId() const
{
  int id = currentId();

  if (id == FinalPage) {
    id = -1;
  } else {
    ++id;
  }

  //// Reset "finishing" if not on FinalPage
  //if (id != FinalPage) {
  //  finishing = false;
  //}

  //switch (currentId())
  //{
  //  // Intro Page
  //  case IntroPage:
  //  id = ServerPage;
  //  break;

  //  // Server Page
  //  case ServerPage:
  //  if (pageValid)
  //  {
  //    if (ui->createUserRadio->isChecked()) {
  //      id = CreateUserPage;
  //    } else if (ui->loginRadio->isChecked()) {
  //      id = LoginPage;
  //    }
  //  }
  //  break;

  //  // Create User Page
  //  case CreateUserPage:
  //  if (pageValid)
  //  {
  //    id = FinalPage;
  //  }
  //  break;

  //  // Login Page
  //  case LoginPage:
  //  if (pageValid)
  //  {
  //    id = FinalPage;
  //  }
  //  break;

  //  // Final Page
  //  case FinalPage:
  //  if (!finishing)
  //  {
  //    finishing = true;
  //    if (ui->createUserRadio->isChecked()) {
  //      emit createUser();
  //    } else {
  //      emit login();
  //    }
  //  }
  //  id = -1;
  //  break;
  //}

  return id;
}

//bool NewClientWizard::confirmServerPage() const
//{
//  bool valid = true;
//
//  // Reset
//  ui->hostError->hide();
//
//  // Check for valid entries
//  if (ui->hostEdit->text().isEmpty())
//  {
//    ui->hostError->show();
//    valid = false;
//  }
//
//  return valid;
//}
//
//bool NewClientWizard::confirmCreateUserPage() const
//{
//  bool valid = true;
//
//  // Reset
//  ui->createEmailError->hide();
//  ui->createPasswordError1->hide();
//  ui->createPasswordError2->hide();
//  ui->nameError->hide();
//
//  // Check for valid entries
//  if (ui->fnameEdit->text().isEmpty() &&
//      ui->mnameEdit->text().isEmpty() &&
//      ui->lnameEdit->text().isEmpty())
//  {
//    ui->hostError->show();
//    valid = false;
//  }
//
//  if (ui->createEmailEdit->text().isEmpty())
//  {
//    ui->createEmailError->show();
//    valid = false;
//  }
//
//  if (ui->createPasswordEdit1->text().isEmpty())
//  {
//    ui->createPasswordError1->show();
//    valid = false;
//  }
//
//  if (ui->createPasswordEdit1->text() != ui->createPasswordEdit2->text())
//  {
//    ui->createPasswordError2->show();
//    valid = false;
//  }
//
//  return valid;
//}
//
//bool NewClientWizard::confirmLoginPage() const
//{
//  bool valid = true;
//
//  // Reset
//  ui->loginEmailError->hide();
//  ui->loginPasswordError->hide();
//
//  // Check for valid entries
//  if (ui->loginEmailEdit->text().isEmpty())
//  {
//    ui->loginEmailError->show();
//    valid = false;
//  }
//
//  if (ui->loginPasswordEdit->text().isEmpty())
//  {
//    ui->loginPasswordError->show();
//    valid = false;
//  }
//
//  return valid;
//}

void NewClientWizard::clientError(QString err)
{

}

void NewClientWizard::clientStatusChanged(int status)
{

}
