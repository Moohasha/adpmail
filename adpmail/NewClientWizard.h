#ifndef NEWCLIENTWIZARD_H
#define NEWCLIENTWIZARD_H

#include <QWizard>

class Client;
namespace Ui {
class NewClientWizard;
}

class NewClientWizard : public QWizard
{
  Q_OBJECT

public:
  explicit NewClientWizard(Client& client, QWidget *parent = 0);
  ~NewClientWizard();

private:
  void next();
  int nextId() const;

  //bool confirmServerPage() const;
  //bool confirmCreateUserPage() const;
  //bool confirmLoginPage() const;

private slots:
  void clientError(QString);
  void clientStatusChanged(int status);

private:
  enum
  { // These values are based on the order of the pages created in designer.
    // If the order of pages changes, this enum needs to be updated.
    IntroPage,
    IntroPage2,
    IntroPage3,
    IntroPage4,
    IntroPage5,
    FinalPage
  };

  Client& client;
  Ui::NewClientWizard *ui;
  bool pageValid;
  mutable bool finishing;
};

#endif // NEWCLIENTWIZARD_H
