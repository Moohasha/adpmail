#include <QtCore/QCoreApplication>
#include <QtCore/QEvent>
#include <QtNetwork/QTcpSocket>
#include <QtNetwork/QHostAddress>
#include <QMessageBox>
#include "Client.h"
#include "ClientDataBaseHelper.h"

class ConnectEvent : public QEvent
{
public:
  static const int TYPE = QEvent::User+1;
  ConnectEvent(QString host, unsigned int port)
    : QEvent((QEvent::Type)TYPE), host(host), port(port) {}
  QString host;
  unsigned int port;
};

class SendEvent : public QEvent
{
public:
  static const int TYPE = QEvent::User+2;
  SendEvent(msgs::MSG* msg): QEvent((QEvent::Type)TYPE) 
  {
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_4_0);
    stream << (quint32)0; // save space for the size
    msg->encode(stream); // then encode the message
    if (!data.isNull()) 
    {
      stream.device()->seek(0);
      stream << (quint32)(data.size()-sizeof(quint32));
    }
  }
  
  QByteArray data;
};



Client::Client(QObject *parent) :
  QThread(parent),
  socket(0),
  blockSize(0),
  lastAction(None)
{
}

QString Client::getHost() const
{
  if (socket && socket->isOpen()) {
    return socket->peerAddress().toString();
  }
  return QString::null;
}

void Client::start(QString host, unsigned int port)
{
  QThread::start(); // start the thread
  if (!host.isEmpty()) {
    connect(host, port);// connect to the specified host
  }
}

void Client::connect(QString host, unsigned int port)
{
  qApp->postEvent(this, new ConnectEvent(host, port));
}

void Client::sendLogin(msgs::LOGIN* msg)
{
  lastAction = RequestLogin;
  send(msg);
}

void Client::sendChangePassword(msgs::CHG_PWD* msg)
{
  lastAction = ChangePassword;
  send(msg);
}

void Client::sendMail(msgs::MAIL* msg)
{
  lastAction = SendMail;
  send(msg);
}

void Client::sendMailReq(msgs::MAIL* msg)
{
  lastAction = RequestMail;
  send(msg);
}

void Client::sendOther(msgs::MSG* msg)
{
  send(msg);
}

void Client::send(msgs::MSG* msg)
{
  qApp->postEvent(this, new SendEvent(msg));
}

void Client::run()
{
  // Setup the client socket on the thread
  socket = new QTcpSocket;

  // Connect to the socket's readyRead signal so we'll know where there's new
  // data to read.
  QObject::connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));

  // Connect to the socket's disconnected signal so that we can know if the 
  // server gets disconnected
  QObject::connect(socket, SIGNAL(disconnected()), this, SLOT(serverDisconnected()));

  exec();
}

void Client::handleOk(QByteArray data)
{
  // OK message has no content, just check what state we were in (why we were 
  // expecting an OK)
  switch (lastAction)
  {
  case RequestCreateUser:
  case RequestLogin:
    emit connectionStatusChanged(LoggedIn);
    break;

  case ChangePassword:
    emit passwordChangeComplete();
    break;
  }

  // Reset last action
  lastAction = None;
}

void Client::handleMail(QByteArray data)
{
  msgs::MAIL msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    QString err;
    bool saved = false;

    for (int i = 0; i < 3; ++i)
    {
      // Write this mail to the database
      if (cdbh::saveMail(msg, err))
      {
        // Emit signal indicating that a new Mail message was received.  Include
        // the uid of the Mail for reference
        emit receivedMail(msg.uid);
        saved = true;
        break;
      }
      else
      {
        // Sleep a short period and try again
        msleep(10);
      }
    }

    if (!saved) {
      emit errorMessage("Received new mail, but could not open it: " + err);
    }
  }
}

void Client::handleClients(QByteArray data)
{
  // msg has to be allocated on the heap since it will be emited in a signal 
  // from a background thread.  Otherwise, it will go out of scope before the
  // slot receives it.
  msgs::CLIENTS* msg = new msgs::CLIENTS;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg->decode(stream))
  {
    // Emit signal with the list of client names
    emit receivedClients(msg);
  }
}

void Client::handleBad(QByteArray data)
{
  msgs::BAD msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // Check what state we were in.  That is, what we were trying to do that
    // just failed.
    QString err;

    switch (lastAction)
    {
    case RequestCreateUser:
      err = "Could not create user account: ";
      break;

    case RequestLogin:
      err = "Login failed: ";
      break;
    }

    // Emit the error message that was returned.
    emit errorMessage(err + msg.err);    
  }

  // Reset last action
  lastAction = None;
}

bool Client::event(QEvent* e)
{
  if (e->type() == ConnectEvent::TYPE)
  {
    ConnectEvent* ce = static_cast<ConnectEvent*>(e);
    QString err;
    
    if (socket)
    {
      // Clone any previous connection
      socket->close();

      // Attempt to connect to host
      socket->connectToHost(ce->host, ce->port);
      if (socket->waitForConnected(1000)) 
      {
        emit connectionStatusChanged(Connected);
      } else {
        emit errorMessage("Could not connect to host: " + socket->errorString());
      }
    }

    // Display error message
    if (!err.isNull()) {
      qDebug() << "Client Error: " << err;
    }
    return true;
  }
  else if (e->type() == SendEvent::TYPE)
  {
    // Send the message data over the socket
    SendEvent* se = static_cast<SendEvent*>(e);
    if (!se->data.isNull()) {
      socket->write(se->data);
    }
  }

  return false;
}

void Client::readSocket()
{
  bool moreAvail = false;
  do
  {
    // If blockSize is 0, we're starting a new message
    if (blockSize == 0)
    {
      // Make sure we have at least enough data to read the block size
      quint64 bytesAvail = socket->bytesAvailable();
      if (socket->bytesAvailable() < (int)sizeof(quint32)) {
        return;
      }

      // Read the block size (message size)
      QDataStream stream(socket);
      stream.setVersion(QDataStream::Qt_4_0);
      stream >> blockSize;

      // If there are more bytes available than the next block size, flag that
      // we need to read more when we're done.
      moreAvail = (bytesAvail > blockSize);
    }

    // Keep returning until we've received the entire message
    if (blockSize == 0 || socket->bytesAvailable() < blockSize) {
      return;
    }

    // Read the message
    QByteArray data = socket->read(blockSize);
    QDataStream stream(&data, QIODevice::ReadOnly);
    blockSize = 0;

    // Create a default message to read the message type
    msgs::MSG msg;
    if (msg.decode(stream))
    {
      // TEMP
      qDebug() << "DEBUG: Received message type " << msgs::typeToString(msg.type);

      switch (msg.type)
      {
      case msgs::Ok:
        handleOk(data);
        break;

      case msgs::Bad:
        handleBad(data);
        break;

      case msgs::Mail:
        handleMail(data);
        break;

      case msgs::Clients:
        handleClients(data);
        break;
      }
    }
  } while (moreAvail);
}

void Client::serverDisconnected()
{
  qDebug() << "Server disconnected.";
  emit connectionStatusChanged(NotConnected);

  // TODO: Attempt to reconnect to the server
}
