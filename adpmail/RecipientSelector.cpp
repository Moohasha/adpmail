#include "RecipientSelector.h"
#include "ui_RecipientSelector.h"

RecipientSelector::RecipientSelector(const QStringList& userNames, QWidget* parent) :
  QDialog(parent),
  ui(new Ui::RecipientSelector),
  mUserNames(userNames)
{
  ui->setupUi(this);
}

void RecipientSelector::setRecipients(const QStringList& names)
{
  // Clear the user list
  ui->usersList->clear();

  // Remove recipient names from user names
  QStringList usersCopy(mUserNames);
  foreach (const QString& name, names) {
    usersCopy.removeOne(name);
  }

  // Add recipients to recipient list
  ui->recipList->addItems(names);
  ui->recipList->sortItems();

  // Add remaining users to user list
  ui->usersList->addItems(usersCopy);
  ui->usersList->sortItems();

  updateButtons();
}

QStringList RecipientSelector::getRecipients() const
{
  QStringList names;

  // Get all names in the recipient list
  for (int i = 0; i < ui->recipList->count(); ++i) {
    names << ui->recipList->item(i)->text();
  }

  return names;
}

RecipientSelector::~RecipientSelector()
{
  delete ui;
}

void RecipientSelector::copyRight()
{
  foreach (QListWidgetItem* item, ui->usersList->selectedItems())
  {
    // Remove the name from the user list
    ui->usersList->takeItem(ui->usersList->row(item));

    // Add to the recipient list
    ui->recipList->addItem(item);
  }

  // Resort recipient list
  ui->recipList->sortItems();

  // Enable/Disable buttons
  updateButtons();
}

void RecipientSelector::copyLeft()
{
  foreach (QListWidgetItem* item, ui->recipList->selectedItems())
  {
    // Remove the name from the recipient list
    ui->recipList->takeItem(ui->recipList->row(item));

    // Add to the user list
    ui->usersList->addItem(item);
  }

  // Resort user list
  ui->usersList->sortItems();

  // Enable/Disable buttons
  updateButtons();
}

void RecipientSelector::copyAllRight()
{
  // Move all names from user list to recipient list
  while (ui->usersList->count() > 0)
  {
    QListWidgetItem* item = ui->usersList->takeItem(0);
    ui->recipList->addItem(item);
  }

  // Resort recipient list
  ui->recipList->sortItems();

  // Enable/Disable buttons
  updateButtons();
}

void RecipientSelector::copyAllLeft()
{
  // Move all names from recipient list to user list
  while (ui->recipList->count() > 0)
  {
    QListWidgetItem* item = ui->recipList->takeItem(0);
    ui->usersList->addItem(item);
  }

  // Resort user list
  ui->usersList->sortItems();

  // Enable/Disable buttons
  updateButtons();
}

void RecipientSelector::updateButtons()
{
  // Enable/Disable buttons
  ui->copyLeftButton->setEnabled(ui->recipList->count() > 0);
  ui->copyAllLeftButton->setEnabled(ui->recipList->count() > 0);
  ui->copyRightButton->setEnabled(ui->usersList->count() > 0);
  ui->copyAllRightButton->setEnabled(ui->usersList->count() > 0);
}
