#ifndef MAILMETA_H
#define MAILMETA_H
#include <QtCore/QString>
#include <QtCore/QDateTime>

// This struct represents the meta data associated with a Mail message.  It is
// contained in a struct so that if it grows or changes in some way, we don't 
// have to update the function signatures that use it.
struct MailMeta
{
  QString from;
  QString subject;
  QDateTime sent_time;
  bool has_read;
  bool has_attachment;
};

#endif // MAILMETA_H
