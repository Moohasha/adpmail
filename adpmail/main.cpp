#include "MainWindow.h"
#include <QApplication>

// RELEASE
#ifndef _DEBUG
#include <Windows.h>

int main(int argc, char* argv[]);

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
  int argc = 1;
  char* argv[] = {lpCmdLine};
  return main(argc, argv);
}

#endif // _DEBUG
// RELEASE


int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  MainWindow w;
  w.init();
  
  return a.exec();
}