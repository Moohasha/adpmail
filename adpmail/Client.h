#ifndef CLIENT_H
#define CLIENT_H

#include <QtCore/QThread>
#include "../common.h"

class QTcpSocket;

enum ConnectionStatus
{
  NotConnected,
  Connected,
  LoggedIn,
};

class Client : public QThread
{
  Q_OBJECT

signals:
  void connectionStatusChanged(int connectionStatus);
  void errorMessage(QString msg);
  void receivedMail(unsigned int uid);
  void passwordChangeComplete();
  void receivedClients(msgs::CLIENTS* msg);

public:
  explicit Client(QObject *parent = 0);
  
  QString getHost() const;
  void start(QString host = QString::null, unsigned int port = SERVER_PORT);
  void connect(QString host, unsigned int port = SERVER_PORT);
  void sendLogin(msgs::LOGIN* msg);
  void sendChangePassword(msgs::CHG_PWD* msg);
  void sendMail(msgs::MAIL* msg);
  void sendMailReq(msgs::MAIL* msg);
  void sendOther(msgs::MSG* msg);

private:
  void send(msgs::MSG* msg);
  void run();
  void handleOk(QByteArray);
  void handleBad(QByteArray);
  void handleMail(QByteArray);
  void handleClients(QByteArray);

private slots:
  bool event(QEvent*);
  void readSocket();
  void serverDisconnected();

private:
  enum LastAction
  {
    None,
    RequestCreateUser,
    RequestLogin,
    ChangePassword,
    SendMail,
    RequestMail
  };

  QTcpSocket* socket;
  quint32 blockSize;
  LastAction lastAction;
};

#endif // CLIENT_H
