#include "TextEdit.h"
#include <QtGui/QApplication>
#include <QtGui/QClipboard>
#include <QtGui/QComboBox>
#include <QtGui/QAction>
#include <QtGui/QFontComboBox>
#include <QtGui/QToolbar>
#include <QtGui/QColorDialog>
#include <QtGui/QTextList>

TextEdit::TextEdit(QWidget* parent)
  : QTextEdit(parent)
{
  setupEditActions();
  setupTextActions();

  connect(this, SIGNAL(currentCharFormatChanged(QTextCharFormat)),
    this, SLOT(onCurrentCharFormatChanged(QTextCharFormat)));
  connect(this, SIGNAL(cursorPositionChanged()),
    this, SLOT(onCursorPositionChanged()));

  fontChanged(font());
  colorChanged(QTextEdit::textColor());
  alignmentChanged(alignment());

  // Connect actions to undo() and redo()
  connect(document(), SIGNAL(undoAvailable(bool)), actionUndo, SLOT(setEnabled(bool)));
  connect(document(), SIGNAL(redoAvailable(bool)), actionRedo, SLOT(setEnabled(bool)));
  connect(actionUndo, SIGNAL(triggered()), this, SLOT(undo()));
  connect(actionRedo, SIGNAL(triggered()), this, SLOT(redo()));
  actionUndo->setEnabled(document()->isUndoAvailable());
  actionRedo->setEnabled(document()->isRedoAvailable());


//#ifndef QT_NO_CLIPBOARD
//  connect(QApplication::clipboard(), SIGNAL(dataChanged()), this, SLOT(clipboardDataChanged()));
//#endif
}

TextEdit::~TextEdit(void)
{
}

void TextEdit::setupEditActions()
{
  // Undo 
  actionUndo = new QAction("&Undo", this);
  actionUndo->setShortcut(QKeySequence::Undo);

  // Redo
  actionRedo = new QAction("&Redo", this);
  actionRedo->setShortcut(QKeySequence::Redo);
}

void TextEdit::setupTextActions()
{
  toolbar = new QToolBar(this);

  // Font
  comboFont = new QFontComboBox(this);
  connect(comboFont, SIGNAL(activated(QString)), this, SLOT(textFamily(QString)));
  toolbar->addWidget(comboFont);

  // Font Size
  comboSize = new QComboBox(this);
  comboSize->setObjectName("comboSize");
  comboSize->setEditable(true);
  QFontDatabase db;
  foreach(int size, db.standardSizes()) {
    comboSize->addItem(QString::number(size));
  }
  connect(comboSize, SIGNAL(activated(QString)), this, SLOT(textSize(QString)));
  comboSize->setCurrentIndex(
    comboSize->findText(QString::number(QApplication::font().pointSize())));
  toolbar->addWidget(comboSize);

  // Color
  QPixmap pix(16, 16);
  pix.fill(Qt::black);
  actionTextColor = new QAction(pix, tr("&Color..."), this);
  connect(actionTextColor, SIGNAL(triggered()), this, SLOT(textColor()));
  toolbar->addAction(actionTextColor);

  // Styles
  comboStyle = new QComboBox(this);
  comboStyle->addItem("Standard");
  comboStyle->addItem("Bullet List (Disc)");
  comboStyle->addItem("Bullet List (Circle)");
  comboStyle->addItem("Bullet List (Square)");
  comboStyle->addItem("Ordered List (Decimal)");
  comboStyle->addItem("Ordered List (Alpha lower)");
  comboStyle->addItem("Ordered List (Alpha upper)");
  connect(comboStyle, SIGNAL(activated(int)), this, SLOT(textStyle(int)));
  toolbar->addWidget(comboStyle);

  // Bold
  actionTextBold = new QAction("B", this);
  actionTextBold->setShortcut(Qt::CTRL + Qt::Key_B);
  QFont bold;
  bold.setBold(true);
  actionTextBold->setFont(bold);
  connect(actionTextBold, SIGNAL(triggered()), this, SLOT(textBold()));
  actionTextBold->setCheckable(true);
  toolbar->addAction(actionTextBold);

  // Italics
  actionTextItalic = new QAction("I", this);
  actionTextItalic->setShortcut(Qt::CTRL + Qt::Key_I);
  QFont italic;
  italic.setItalic(true);
  actionTextItalic->setFont(italic);
  connect(actionTextItalic, SIGNAL(triggered()), this, SLOT(textItalic()));
  actionTextItalic->setCheckable(true);
  toolbar->addAction(actionTextItalic);

  // Underline
  actionTextUnderline = new QAction("U", this);
  actionTextUnderline->setShortcut(Qt::CTRL + Qt::Key_U);
  QFont underline;
  underline.setUnderline(true);
  actionTextUnderline->setFont(underline);
  connect(actionTextUnderline, SIGNAL(triggered()), this, SLOT(textUnderline()));
  actionTextUnderline->setCheckable(true);
  toolbar->addAction(actionTextUnderline);

  // Alignments
  QActionGroup *grp = new QActionGroup(this);
  connect(grp, SIGNAL(triggered(QAction*)), this, SLOT(textAlign(QAction*)));
  actionAlignLeft = new QAction("L", grp);
  actionAlignCenter = new QAction("C", grp);
  actionAlignRight = new QAction("R", grp);
  actionAlignJustify = new QAction("J", grp);
  toolbar->addActions(grp->actions());

  actionAlignLeft->setShortcut(Qt::CTRL + Qt::Key_L);
  actionAlignLeft->setCheckable(true);
  actionAlignCenter->setShortcut(Qt::CTRL + Qt::Key_E);
  actionAlignCenter->setCheckable(true);
  actionAlignRight->setShortcut(Qt::CTRL + Qt::Key_R);
  actionAlignRight->setCheckable(true);
  actionAlignJustify->setShortcut(Qt::CTRL + Qt::Key_J);
  actionAlignJustify->setCheckable(true);
}

void TextEdit::fontChanged(const QFont &f)
{
  comboFont->setCurrentIndex(comboFont->findText(QFontInfo(f).family()));
  comboSize->setCurrentIndex(comboSize->findText(QString::number(f.pointSize())));
  actionTextBold->setChecked(f.bold());
  actionTextItalic->setChecked(f.italic());
  actionTextUnderline->setChecked(f.underline());
}

void TextEdit::colorChanged(const QColor &c)
{
  QPixmap pix(16, 16);
  pix.fill(c);
  actionTextColor->setIcon(pix);
}

void TextEdit::alignmentChanged(Qt::Alignment a)
{
  if (a & Qt::AlignLeft) {
    actionAlignLeft->setChecked(true);
  } else if (a & Qt::AlignHCenter) {
    actionAlignCenter->setChecked(true);
  } else if (a & Qt::AlignRight) {
    actionAlignRight->setChecked(true);
  } else if (a & Qt::AlignJustify) {
    actionAlignJustify->setChecked(true);
  }
}

void TextEdit::textBold()
{
  QTextCharFormat fmt;
  fmt.setFontWeight(actionTextBold->isChecked() ? QFont::Bold : QFont::Normal);
  mergeFormatOnWordOrSelection(fmt);
}

void TextEdit::textUnderline()
{
  QTextCharFormat fmt;
  fmt.setFontUnderline(actionTextUnderline->isChecked());
  mergeFormatOnWordOrSelection(fmt);
}

void TextEdit::textItalic()
{
  QTextCharFormat fmt;
  fmt.setFontItalic(actionTextItalic->isChecked());
  mergeFormatOnWordOrSelection(fmt);
}

void TextEdit::textFamily(const QString &f)
{
  QTextCharFormat fmt;
  fmt.setFontFamily(f);
  mergeFormatOnWordOrSelection(fmt);
}

void TextEdit::textSize(const QString &p)
{
  qreal pointSize = p.toFloat();
  if (p.toFloat() > 0) 
  {
    QTextCharFormat fmt;
    fmt.setFontPointSize(pointSize);
    mergeFormatOnWordOrSelection(fmt);
  }
}

void TextEdit::textStyle(int styleIndex)
{
  QTextCursor cursor = textCursor();

  if (styleIndex != 0) 
  {
    QTextListFormat::Style style = QTextListFormat::ListDisc;

    switch (styleIndex) {
    default:
    case 1:
      style = QTextListFormat::ListDisc;
      break;
    case 2:
      style = QTextListFormat::ListCircle;
      break;
    case 3:
      style = QTextListFormat::ListSquare;
      break;
    case 4:
      style = QTextListFormat::ListDecimal;
      break;
    case 5:
      style = QTextListFormat::ListLowerAlpha;
      break;
    case 6:
      style = QTextListFormat::ListUpperAlpha;
      break;
    }

    cursor.beginEditBlock();

    QTextBlockFormat blockFmt = cursor.blockFormat();

    QTextListFormat listFmt;

    if (cursor.currentList()) {
      listFmt = cursor.currentList()->format();
    } else {
      listFmt.setIndent(blockFmt.indent() + 1);
      blockFmt.setIndent(0);
      cursor.setBlockFormat(blockFmt);
    }

    listFmt.setStyle(style);

    cursor.createList(listFmt);

    cursor.endEditBlock();
  } else {
    // ####
    QTextBlockFormat bfmt;
    bfmt.setObjectIndex(-1);
    cursor.mergeBlockFormat(bfmt);
  }
}

void TextEdit::textColor()
{
  QColor col = QColorDialog::getColor(QTextEdit::textColor(), this);
  if (!col.isValid()) {
    return;
  }
  QTextCharFormat fmt;
  fmt.setForeground(col);
  mergeFormatOnWordOrSelection(fmt);
  colorChanged(col);
}

void TextEdit::textAlign(QAction *a)
{
  if (a == actionAlignLeft) {
    setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
  } else if (a == actionAlignCenter) {
    setAlignment(Qt::AlignHCenter);
  } else if (a == actionAlignRight) {
    setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
  } else if (a == actionAlignJustify) {
    setAlignment(Qt::AlignJustify);
  }
}

void TextEdit::onCurrentCharFormatChanged(const QTextCharFormat &format)
{
  fontChanged(format.font());
  colorChanged(format.foreground().color());
}

void TextEdit::onCursorPositionChanged()
{
  alignmentChanged(alignment());
}

//void TextEdit::clipboardDataChanged()
//{
//#ifndef QT_NO_CLIPBOARD
//  if (const QMimeData *md = QApplication::clipboard()->mimeData())
//    actionPaste->setEnabled(md->hasText());
//#endif
//}

void TextEdit::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
  QTextCursor cursor = textCursor();
  if (!cursor.hasSelection())
    cursor.select(QTextCursor::WordUnderCursor);
  cursor.mergeCharFormat(format);
  mergeCurrentCharFormat(format);
}