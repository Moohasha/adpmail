#ifndef TEXTEDIT_H
#define TEXTEDIT_H
#include <QtGui/QTextEdit>

// TODO: Move this to Compose Dialog
// http://qt-project.org/doc/qt-4.8/demos-textedit.html

class QAction;
class QComboBox;
class QFontComboBox;
class QToolBar;


class TextEdit : public QTextEdit
{
  Q_OBJECT

public:
  TextEdit(QWidget* parent = 0);
  ~TextEdit(void);

public:
  // These actions/widgets are owned by the TextEdit so that it can modify them
  // based on the current formatting, but they are made public so that the 
  // parent widget can place them in toolbars and customize their layout.
  QAction* actionTextBold;
  QAction* actionTextUnderline;
  QAction* actionTextItalic;
  QAction* actionTextColor;
  QAction* actionAlignLeft;
  QAction* actionAlignCenter;
  QAction* actionAlignRight;
  QAction* actionAlignJustify;
  QAction* actionUndo;
  QAction* actionRedo;

  QComboBox* comboStyle;
  QFontComboBox* comboFont;
  QComboBox* comboSize;

  QToolBar* toolbar;

private slots:
  void textBold();
  void textUnderline();
  void textItalic();
  void textFamily(const QString &f);
  void textSize(const QString &p);
  void textStyle(int styleIndex);
  void textColor();
  void textAlign(QAction *a);

  void onCurrentCharFormatChanged(const QTextCharFormat &format);
  void onCursorPositionChanged();
  //void clipboardDataChanged();

private:
  void setupEditActions();
  void setupTextActions();
  void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
  void fontChanged(const QFont &f);
  void colorChanged(const QColor &c);
  void alignmentChanged(Qt::Alignment a);
};

#endif // TEXTEDIT_H