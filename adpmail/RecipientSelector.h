#ifndef RECIPIENTSELECTOR_H
#define RECIPIENTSELECTOR_H

#include <QDialog>

namespace Ui {
class RecipientSelector;
}

class RecipientSelector : public QDialog
{
  Q_OBJECT
  
public:
  explicit RecipientSelector(const QStringList& userNames, QWidget *parent = 0);
  ~RecipientSelector();
  
  void setRecipients(const QStringList& names);
  QStringList getRecipients() const;

private slots:
  void copyRight();
  void copyLeft();
  void copyAllRight();
  void copyAllLeft();

private:
  void updateButtons();

private:
  Ui::RecipientSelector *ui;
  QStringList mUserNames;
};

#endif // RECIPIENTSELECTOR_H
