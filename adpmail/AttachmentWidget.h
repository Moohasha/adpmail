#ifndef ATTACHMENTWIDGET_H
#define ATTACHMENTWIDGET_H

#include <QFrame>

namespace Ui {class AttachmentWidget;}

class AttachmentWidget : public QFrame
{
  Q_OBJECT

public:
  explicit AttachmentWidget(const QString& path, QWidget *parent = 0);
  ~AttachmentWidget();

  QString getFilename() const;
  QString getPath() const;
  QByteArray getFileData() const;

signals:
  void removeClicked();

private:
  Ui::AttachmentWidget* ui;
  QString mPath;
  QByteArray mData;
};

#endif // ATTACHMENTWIDGET_H
