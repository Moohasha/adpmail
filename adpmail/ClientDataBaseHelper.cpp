#include <QtSql>
#include "ClientDataBaseHelper.h"
#include "MailMeta.h"

namespace cdbh
{
  // --------------------------------------------------------------------------
  // saveMail
  // --------------------------------------------------------------------------
  bool saveMail(msgs::MAIL& mail, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Build query to store mail in database
    int i = 0;
    query.prepare("INSERT INTO Mail (uid, category, has_read, has_att, to_emails, cc_emails, from_email, subject, sent_time, body)"
                  "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
    query.bindValue(i++, mail.uid);
    query.bindValue(i++, mail.category);
    query.bindValue(i++, mail.has_read);
    query.bindValue(i++, mail.attachments.count() > 0);
    query.bindValue(i++, mail.to.join(";"));
    query.bindValue(i++, mail.cc.join(";"));
    query.bindValue(i++, mail.from);
    query.bindValue(i++, mail.subject);
    query.bindValue(i++, mail.sent_time);
    query.bindValue(i++, mail.body);
    if (query.exec())
    {
      // Save attachments
      QMap<QString, QByteArray>::const_iterator citr = mail.attachments.begin();
      for (; citr != mail.attachments.end(); ++citr)
      {
        QString filename = citr.key();

        // Generate random uuid
        QUuid att_uid = QUuid::createUuid();

        // Write attachment to file
        QFile file(ATTACHMENTS_PATH + att_uid.toString());
        if (file.open(QFile::WriteOnly))
        {
          file.write(citr.value());
          file.close();
          quint32 size = (quint32)file.size();

          // Write to Attachment table
          query.prepare("INSERT INTO Attachment (uid, filename, size) VALUES (?, ?, ?)");
          query.bindValue(0, att_uid.toString());
          query.bindValue(1, filename);
          query.bindValue(2, size);
          if (query.exec() == false) {
            qDebug() << "Failed to write attachment to database: " << query.lastError();
            file.remove();
            continue;
          }

          // Write to MailAtt table
          query.prepare("INSERT INTO MailAtt (att_id, mail_id) VALUES (?, ?)");
          query.bindValue(0, att_uid.toString());
          query.bindValue(1, mail.uid);
          if (query.exec() == false) {
            qDebug() << "Failed to associate attachment to mail: " << query.lastError();
          }
        }
      }

      qDebug() << "DEBUG: Stored mail with subject " << mail.subject;
      retval = true;
    }
    else
    {
      err = query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getMailMeta
  // --------------------------------------------------------------------------
  bool getMailMeta(unsigned int uid, MailMeta& meta, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Build query to retrieve the meta data for the specified Mail
    query.prepare("SELECT from_email, subject, sent_time, has_read, has_att FROM Mail WHERE uid=?");
    query.bindValue(0, uid);
    if (query.exec() && query.next())
    {
      meta.from = query.value(0).toString();
      meta.subject = query.value(1).toString();
      meta.sent_time = query.value(2).toDateTime();
      meta.has_read = query.value(3).toBool();
      meta.has_attachment = query.value(4).toBool();
      retval = true;
    }
    else
    {
      err = "Mail not found: " + query.lastError().text();
    }

    return retval;
  }
  
  // --------------------------------------------------------------------------
  // getMail
  // --------------------------------------------------------------------------
  bool getMail(unsigned int uid, msgs::MAIL& mail, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Build query to retrieve the meta data for the specified Mail
    query.prepare("SELECT category, to_emails, cc_emails, from_email, subject, sent_time, body FROM Mail WHERE uid=?");
    query.bindValue(0, uid);
    if (query.exec() && query.next())
    {
      int i = 0;
      mail.uid        = uid;
      mail.category   = query.value(i++).toInt();
      mail.to         = query.value(i++).toString().split(";", QString::SkipEmptyParts);
      mail.cc         = query.value(i++).toString().split(";", QString::SkipEmptyParts);
      mail.from       = query.value(i++).toString();
      mail.subject    = query.value(i++).toString();
      mail.sent_time  = query.value(i++).toDateTime();
      mail.body       = query.value(i++).toString();

      // Look up attachments.  Do select on Attachment table for attachments
      // associated with this Mail ID in the MailAtt table.
      query.prepare("SELECT filename, uid FROM Attachment INNER JOIN MailAtt "
                    "WHERE Attachment.uid = MailAtt.att_id AND MailAtt.mail_id=?");
      query.bindValue(0, uid);
      if (query.exec())
      {
        while (query.next())
        {
          QString filename = query.value(0).toString();
          QString att_id = query.value(1).toString();
          QFile file(ATTACHMENTS_PATH + att_id);

          // We don't read in the file, because at this point we really only
          // care about whether or not it exists.  It will get read in if/when
          // the user is ready to open it.
          if (file.exists())
          {
            // In this case, we're only storing the file uuid, not the actual
            // file.  A little hacky, but it lets us work with what we have.
            mail.attachments.insert(filename, att_id.toLatin1());
          }
        }
      }
      retval = true;
    }
    else
    {
      err = "Mail not found: " + query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getAllCachedMailUids
  // --------------------------------------------------------------------------
  bool getAllCachedMailUids(QList<unsigned int>& uids, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Select all UIDs from the Mail table.
    query.prepare("SELECT uid FROM Mail");
    if (query.exec())
    {
      while (query.next()) {
        uids << query.value(0).toUInt();
      }
      retval = true;
    }
    else
    {
      err = query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // deleteMail
  // --------------------------------------------------------------------------
  bool deleteMail(unsigned int uid, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Delete the row from the database which has this UID as the primary key.
    query.prepare("DELETE FROM Mail WHERE uid=?");
    query.bindValue(0, uid);
    retval = query.exec();

    // Also look up file attachments associated with the mail so they can
    // be deleted
    query.prepare("SELECT att_id FROM MailAtt WHERE mail_id=?");
    query.bindValue(0, uid);
    if (query.exec())
    {
      while (query.next())
      {
        QString att_uid = query.value(0).toString();

        // Remove file
        QFile::remove(ATTACHMENTS_PATH + att_uid);

        // Remove from Attachments table
        QSqlQuery query2;
        query2.prepare("DELETE FROM Attachment WHERE uid=?");
        query2.bindValue(0, att_uid);
        if (query2.exec() == false) {
          qDebug() << "Failed to delete attachment " << att_uid
                   << " from database: " << query2.lastError();
        }
      }
    }
    else
    {
      // Note: Simply returning 0 rows will not cause exec() to return
      // false, so this will truely only display errors.
      qDebug() << "Failed to lookup mail attachments: "
               << query.lastError();
    }

    // Now remove from MailAtt table
    query.prepare("DELETE FROM MailAtt WHERE mail_id=?");
    query.bindValue(0, uid);
    if (query.exec() == false) {
      qDebug() << "Failed to delete mail attachments from database: "
               << query.lastError();
    }

    return retval;
  }
  
  // --------------------------------------------------------------------------
  // setMailRead
  // --------------------------------------------------------------------------
  bool setMailRead(unsigned int uid, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Update the row for the specified Mail
    query.prepare("UPDATE Mail SET has_read='1' WHERE uid=?");
    query.bindValue(0, uid);
    retval = query.exec();
    if (!retval) {
      err = query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getNumUnreadMail
  // --------------------------------------------------------------------------
  bool getNumUnreadMail(unsigned int& count, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Get a count of how many Mail messages are unread
    query.prepare("SELECT has_read, COUNT(1) AS 'Total' FROM Mail WHERE has_read='false'");
    retval = query.exec();
    if (retval) {
      query.next();
      count = query.value(1).toUInt();
    } else {
      err = query.lastError().text();
    }

    return retval;
  }
}
