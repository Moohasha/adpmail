#include <QtCore/QDebug>
#include <QtGui/QToolBar>
#include <QtGui/QFileDialog>
#include "ComposeDialog.h"
#include "RecipientSelector.h"
#include "AttachmentWidget.h"
#include "ui_ComposeDialog.h"
#include "..\common.h"

ComposeDialog::ComposeDialog(QString userEmail, QWidget *parent)
  : QDialog(parent),
    ui(new Ui::ComposeDialog),
    userEmail(userEmail)
{
  ui->setupUi(this);
  ui->attachmentScrollArea->hide(); // hide by default
  setAttribute(Qt::WA_DeleteOnClose);
  initEditor(); // initialize the editor actions
}


ComposeDialog::~ComposeDialog(void)
{
  delete ui;
}

void ComposeDialog::init(const msgs::MAIL& mail, Mode mode)
{
  QString subjPrefix;
  switch (mode)
  {
  case ReplyTo:
    ui->toEdit->setText(mail.from);
    subjPrefix = "RE: ";
    break;

  case ReplyToAll:
    {
      // Need to remove self from "to" recipients
      QStringList recipients(mail.to);
      recipients.removeAll(userEmail);
      recipients.removeAll(mail.from); // Make sure the from user isn't already in there
      recipients.prepend(mail.from); // include "from" user at the beginning of the list
      ui->toEdit->setText(recipients.join("; "));
      ui->ccEdit->setText(mail.cc.join("; "));
      subjPrefix = "RE: ";
    } break;

  case Forward:
    subjPrefix = "FWD: ";
    break;
  }

  // Remove any existing "RE: " or "FWD: " from the subject line
  QString subject = mail.subject;
  if (subject.startsWith("RE: ")) {
    subject.remove(QString("RE: ").length());
  } else if (subject.startsWith("FWD: ")) {
    subject.remove(QString("FWD: ").length());
  }

  // Subject
  ui->subjectEdit->setText(subjPrefix + mail.subject);

  qDebug() << "to: " << mail.to;
  qDebug() << "to joined: " << mail.to.join("; ");
  // Body - Include the meta info, like from, to, subject, etc.
  QString body = "<br><br><hr>";
  body += "<b>From:</b> " + mail.from + "<br>";
  body += "<b>Sent:</b> " + mail.sent_time.toString() + "<br>";
  body += "<b>To:</b> " + mail.to.join("; ") + "<br>";
  if (!mail.cc.empty()) {
    body += "<b>Cc:</b> " + mail.cc.join("; ") + "<br>";
  }
  body += "<b>Subject:</b> " + mail.subject + "<br>";
  body += mail.body;

  ui->bodyEdit->setText(body);
  
  ui->bodyEdit->setFocus();
}

void ComposeDialog::setUserList(const QMap<QString, QString>& users)
{
  mUsers = users;
  //ui->toEdit->setCompletionList(users.values());
  //ui->ccEdit->setCompletionList(users.values());
}

void ComposeDialog::toClicked()
{
  QStringList names(parseUserNames(ui->toEdit->text(), true));
  RecipientSelector dialog(mUsers.values(), this);
  dialog.setRecipients(names);
  if (dialog.exec())
  {
    // Update the To field in the dialog
    names = dialog.getRecipients();
    ui->toEdit->setText(names.join("; "));
  }
}

void ComposeDialog::ccClicked()
{
  QStringList names(parseUserNames(ui->ccEdit->text(), true));
  RecipientSelector dialog(mUsers.values(), this);
  dialog.setRecipients(names);
  if (dialog.exec())
  {
    // Update the Cc field in the dialog
    names = dialog.getRecipients();
    ui->ccEdit->setText(names.join("; "));
  }
}

void ComposeDialog::accept()
{
  // Emit a MAIL message so that the whoever receives it can send it to the
  // server.
  msgs::MAIL* mail = new msgs::MAIL;
  mail->to = parseUserNames(ui->toEdit->text(), false);
  mail->cc = parseUserNames(ui->ccEdit->text(), false);
  mail->from = userEmail;
  mail->subject = ui->subjectEdit->text();
  mail->sent_time = QDateTime::currentDateTime();
  mail->body = ui->bodyEdit->toHtml();
  foreach (AttachmentWidget* widget, mAttachments) {
    mail->attachments.insert(widget->getFilename(), widget->getFileData());
  }

  emit sendMail(mail);

  // Call accept on the dialog to finish accepting
  QDialog::accept();
}

void ComposeDialog::addAttachmentClicked()
{
  QStringList paths = QFileDialog::getOpenFileNames(this, "Select Attachment(s)");
  foreach (const QString& path, paths) {
    addAttachment(path);
  }
}

void ComposeDialog::removeAttachmentClicked()
{
  if (AttachmentWidget* widget = qobject_cast<AttachmentWidget*>(sender()))
  {
    // Find this attachment widget and remove it from the list
    QMap<QString, AttachmentWidget*>::iterator itr = mAttachments.begin();
    while (itr != mAttachments.end())
    {
      if (itr.value() == widget)
      {
        mAttachments.erase(itr);
        break;
      }
      ++itr;
    }

    // Delete the attachment widget
    delete widget;

    // If no more attachments, hide scroll area
    if (mAttachments.empty()) {
      ui->attachmentScrollArea->hide();
    }
  }
}

void ComposeDialog::initEditor()
{
  ui->editActionsLayout->insertWidget(0, ui->bodyEdit->toolbar);
}

QStringList ComposeDialog::parseUserNames(const QString& str, bool validate)
{
  // Save list of user names
  QStringList userNames(mUsers.values());

  // Split the string by ;
  QStringList names = str.split(";", QString::SkipEmptyParts);

  QStringList::iterator itr = names.begin();
  while (itr != names.end())
  {
    // Make sure the name is trimmed
    *itr = itr->trimmed();

    // If validating, the name MUST be in the user list
    if (validate)
    {
      // Check for email address
      if (itr->contains("@"))
      {
        // Make sure it's in the user list
        if (mUsers.contains(*itr)) {
          *itr = mUsers.value(*itr); // replace with user name
          ++itr; // advance
        } else {
          itr = names.erase(itr); // unknown email, remove from list
        }
      }
      // Check for user name
      else
      {
        // If the name doesn't exist in the user names list, remove it
        if (userNames.contains(*itr, Qt::CaseInsensitive)) {
          ++itr; // advance
        } else {
          itr = names.erase(itr); // unknown user, erase
        }
      }
    }
    else
    {
      ++itr; // advance
    }
  }

  // Remove duplicates
  names.removeDuplicates();

  return names;
}

void ComposeDialog::addAttachment(const QString& path)
{
  if (!mAttachments.contains(path))
  {
    // Add a new attachment widget
    AttachmentWidget* widget = new AttachmentWidget(path, ui->attachmentScrollContents);
    ui->attachmentsLayout->insertWidget(mAttachments.count(), widget);
    mAttachments.insert(path, widget);

    // Make sure the scroll area is visible
    ui->attachmentScrollArea->show();

    // Connect to the remove signal
    connect (widget, SIGNAL(removeClicked()), this, SLOT(removeAttachmentClicked()));
  }
}
