#-------------------------------------------------
#
# Project created by QtCreator 2013-03-27T21:33:04
#
#-------------------------------------------------

QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = adpmail
TEMPLATE = app


SOURCES += main.cpp\
    MainWindow.cpp \
    Client.cpp \
    ../common.cpp \
    ComposeDialog.cpp \
    NewClientWizard.cpp \
    ClientDataBaseHelper.cpp \
    TextEdit.cpp \
    RecipientSelector.cpp \
    AttachmentWidget.cpp

HEADERS  += MainWindow.h \
    ../common.h \
    Client.h \
    ComposeDialog.h \
    NewClientWizard.h \
    ClientDataBaseHelper.h \
    MailMeta.h \
    TextEdit.h \
    RecipientSelector.h \
    AttachmentWidget.h

FORMS    += MainWindow.ui \
    ChangePasswordDialog.ui \
    ComposeDialog.ui \
    ConnectDialog.ui \
    LoginDialog.ui \
    ../StatusWidget.ui \
    NewClientWizard.ui \
    RecipientSelector.ui \
    AttachmentWidget.ui

RESOURCES += \
    ../resources/resources.qrc
