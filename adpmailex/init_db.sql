-- The table of all users.  A client must login with a valid email and password
-- in order for the server to respond to any queries.
CREATE TABLE IF NOT EXISTS User
(
  email CHAR(128) NOT NULL UNIQUE,
  password CHAR(128) NOT NULL,
  f_name CHAR(32),
  m_name CHAR(8),
  l_name CHAR(32),
  PRIMARY KEY (email)
);


-- A group of one or more Users.  An email addressed to a MailGroup will get
-- send to all Users in the group.
CREATE TABLE IF NOT EXISTS MailGroup
(
  uid INT NOT NULL UNIQUE,
  name CHAR(128) NOT NULL,
  desc CHAR(255),
  PRIMARY KEY (uid)
);


-- Associates Users to a MailGroup.  Users can be in more than one MailGroup.
CREATE TABLE IF NOT EXISTS UserGroup
(
  group_id INT NOT NULL,
  user_id CHAR(128) NOT NULL,
  FOREIGN KEY (group_id) REFERENCES MailGroup(uid),
  FOREIGN KEY (user_id) REFERENCES User(email)
);


-- A single Mail message.  Each Mail message can be shared by all recipients
-- of the Mail via the Inbox table.
CREATE TABLE IF NOT EXISTS Mail
(
  uid INTEGER PRIMARY KEY AUTOINCREMENT,
  category INT NOT NULL,
  to_emails TEXT NOT NULL,
  cc_emails TEXT,
  from_email CHAR(128) NOT NULL,
  subject CHAR(128) NOT NULL,
  sent_time DATETIME NOT NULL,
  body TEXT
);


-- Associates mail from the Mail table to Users.  Multiple Users can share a
-- single Mail message.
CREATE TABLE IF NOT EXISTS Inbox
(
  user_id CHAR(128) NOT NULL,
  mail_id INT NOT NULL,
  has_read BOOLEAN NOT NULL,
  FOREIGN KEY (user_id) REFERENCES User(email),
  FOREIGN KEY (mail_id) REFERENCES Mail(uid)
);

-- Stores references to file attachments that are saved on disk
CREATE TABLE IF NOT EXISTS Attachment
(
  uid CHAR(38) PRIMARY KEY,
  filename CHAR(128) NOT NULL,
  size INT NOT NULL
);

-- Associates attachments in the Attachment table to Mail.  Multiple
-- attachments can be associated to a single Mail message.
CREATE TABLE IF NOT EXISTS MailAtt
(
  att_id  CHAR(38) NOT NULL,
  mail_id INT      NOT NULL,
  FOREIGN KEY (att_id)  REFERENCES Attachment(uid),
  FOREIGN KEY (mail_id) REFERENCES Mail(uid)
);
