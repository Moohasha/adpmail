#include <QtCore/QDateTime>
#include <QtCore/QVariant>
#include <QtNetwork/QTcpSocket>
#include "ServerThread.h"
#include "ServerDataBaseHelper.h"
#include "../common.h"

QSet<ServerThread*> ServerThread::msThreads;
QMutex ServerThread::msMutex;

// ----------------------------------------------------------------------------
// ctor
// ----------------------------------------------------------------------------
ServerThread::ServerThread(int sd)
: socketDescriptor(sd),
  socket(0),
  blockSize(0)
{
  QMutexLocker locker(&msMutex);
  msThreads << this;
}

// ----------------------------------------------------------------------------
// dtor
// ----------------------------------------------------------------------------
ServerThread::~ServerThread()
{
  QMutexLocker locker(&msMutex);
  msThreads.remove(this);
  delete socket;
}

// ----------------------------------------------------------------------------
// send
// ----------------------------------------------------------------------------
void ServerThread::send(msgs::MSG* msg)
{
  if (socket)
  {
    QByteArray data;
    QDataStream stream(&data, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_4_0);
    stream << (quint32)0; // save space for the size
    msg->encode(stream); // then encode the message
    if (!data.isNull())
    {
      qDebug() << "DEBUG: sending message " << msgs::typeToString(msg->type);

      // Write the message size to the beginning of the stream.
      stream.device()->seek(0);
      stream << (quint32)(data.size()-sizeof(quint32));
      socket->write(data);
      socket->flush();
    }
    else
    {
      qDebug() << "ERROR: data to send is null";
    }
  }
}

// ----------------------------------------------------------------------------
// run
// ----------------------------------------------------------------------------
void ServerThread::run()
{
  // Create the socket on the thread and set the socket descriptor
  socket = new QTcpSocket;
  if (!socket->setSocketDescriptor(socketDescriptor))
  {
   emit error(socket->error());
   return;
  }

  // Connect to the socket's readyRead signal so we'll know where there's new
  // data to read.
  connect(socket, SIGNAL(readyRead()), this, SLOT(readSocket()));

  // Connect to the socket's disconnected signal so that we know when the client
  // has been disconnected.
  connect(socket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));

  // Run event loop
  exec();
}

// ----------------------------------------------------------------------------
// handleLogin
// ----------------------------------------------------------------------------
void ServerThread::handleLogin(QByteArray data)
{
  msgs::LOGIN msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // See if the provided username (email) and password are valid
    if (sdbh::attemptLogin(msg.email, msg.password))
    {
      QString err;
      currentEmail = msg.email;
      for (int i = 0; i < 3; ++i) {
        if (sdbh::getUserName(currentEmail, currentUser, err)) {
          break;
        }
      }
      qDebug() << currentUser << " has successfully logged in.";
      msgs::OK ok;
      send(&ok);

      // Return a CLIENTS message of all client names
      msgs::CLIENTS clients;
      if (sdbh::getClients(clients, err))
      {
        send(&clients);
      }
    }
    else
    {
      // Bad username or password
      msgs::BAD bad;
      bad.err = "Invalid username or password.";
      send(&bad);
    }
  }
}

// ----------------------------------------------------------------------------
// 3
// ----------------------------------------------------------------------------
void ServerThread::handleChangePassword(QByteArray data)
{
  msgs::CHG_PWD msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // Confirm that the old password matches.  The easiest way to do this is to
    // call attemptLogin since that's what it does.
    if (sdbh::attemptLogin(currentEmail, msg.old_pwd))
    {
      // Change the user's password to the new one provided
      QString err;
      if (sdbh::changePassword(currentEmail, msg.new_pwd, err))
      {
        msgs::OK ok;
        send(&ok);
      }
      else
      {
        msgs::BAD bad("Failed to change user's password: " + err);
        send(&bad);
      }
    }
    else
    {
      msgs::BAD bad("Old password does not match");
      send(&bad);
    }
  }
  else
  {
    msgs::BAD bad("Could not process Change Password message");
    send(&bad);
  }
}

// ----------------------------------------------------------------------------
// handleMail
// ----------------------------------------------------------------------------
void ServerThread::handleMail(QByteArray data)
{
  msgs::MSG* respMsg = 0;
  msgs::MAIL msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // We want to leave the to and cc fields of the mail as is, but get the email
    // address associated with each name in it.  Convert each name to an email
    // address and store them in a temporary list.
    QSet<QString> recipients = msg.to.toSet() + msg.cc.toSet();
    QStringList to_emails;
    foreach (QString to, recipients)
    {
      bool success = false;
      if (to.contains('@')) {
        to_emails << to_emails; // Already an email
      } else 
      {
        // Convert user name to email
        QString user;
        QString err;
        if (sdbh::getUserEmail(to, user, err)) {
          to_emails << user;
        } else {
          qDebug() << "ERROR: " << err;
        }
      }
    }

    // Set the from field of the mail to whoever the current user is.
    msg.from = currentUser;
    msg.has_read = false; // ensure has_read is false since nobody has received it yet.
    QString err;
    if (sdbh::saveMail(msg, err))
    {
      // Associate this email to each recipient in the "to" and "cc" field
      foreach (QString email, to_emails)
      {
        // For some reason this fails sometimes when it shouldn't.  I think 
        // it's an issue with writing to the database too fast, so try up to
        // three times with a small pause between each.
        for (int i = 0; i < 3; ++i)
        {
          if (!sdbh::addMailToInbox(msg, email, err))
          {
            qDebug() << "Error associating email to recipient "  << email << ": " << err;
            // TODO: Send MAIL or BAD back to client to notify him that the email couldn't
            // be delivered to some recipients.
          }
          else
          {
            qDebug() << "DEBUG: Stored email for " << email;
            break;
          }
          msleep(10);
        }
      }

      // Forward the mail to each recipient currently logged in.  We do this 
      // outside of the above loop because each thread checks to see if its
      // user is in the "to" field of the mail.
      // Note: It is a valid use case for this email to get sent right back to
      // the same user who originally sent it.
      foreach (ServerThread* thread, msThreads) {
        thread->forwardMail(&msg, to_emails);
      }

      // Send OK message
      respMsg = new msgs::OK;
    }
    else
    {
      // Handle failed query
      qDebug() << "Failed to store mail: " << err;
      respMsg = new msgs::BAD("Server failed to store mail: " + err);
    }
  }
  else
  {
    // Return BAD message that there was an error processing the MAIL message
    respMsg = new msgs::BAD("Failed to process Mail message.");
  }

  // If a response message was created, send it.
  if (respMsg)
  {
    send(respMsg);
    delete respMsg;
  }
}

// ----------------------------------------------------------------------------
// handleMailDelete
// ----------------------------------------------------------------------------
void ServerThread::handleMailDelete(QByteArray data)
{
  msgs::MAIL_DEL msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // Try a few times to delete the mail.  Sometimes the database can't be
    // hit too soon back to back and it fails.
    bool passed = false;
    QString err;
    for (int i = 0; i < 3; ++i)
    {
      if (sdbh::deleteMail(msg.uids, currentEmail, err))
      {
        // Send OK response
        msgs::OK ok;
        send(&ok);
        passed = true;
        break;
      }
      else
      {
        qDebug() << "Failed to delete mail: " << err;
        msleep(10);
      }
    }

    // Send BAD response if it failed
    if (!passed) 
    {
      msgs::BAD bad("Server failed to delete mail: " + err);
      send(&bad);
    }
  }
  else
  {
    msgs::BAD bad("Failed to process MailDelete message");
    send(&bad);
  }
}

// ----------------------------------------------------------------------------
// handleMailRequest
// ----------------------------------------------------------------------------
void ServerThread::handleMailRequest(QByteArray data)
{
  msgs::MAIL_REQ msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    QString err;
    QList<unsigned int> newUids;
    if (sdbh::checkForNewMail(currentEmail, msg.uids, newUids, err))
    {
      // If any new uids were returned, lookup the message for each and return it
      foreach (unsigned int uid, newUids)
      {
        // Get this Mail message and send it back to the client
        msgs::MAIL mail;
        if (sdbh::getMail(uid, mail, err)) 
        {
          send(&mail);
        }
      }

      // Send the OK response
      msgs::OK ok;
      send(&ok);
    }
    else
    {
      // Send BAD response
      qDebug() << "Failed to lookup new mail: " << err;
      msgs::BAD bad("Server failed to lookup new mail: " + err);
      send(&bad);
    }
  }
  else
  {
    msgs::BAD bad("Failed to process MailRequest message");
    send(&bad);
  }
}

// ----------------------------------------------------------------------------
// handleMailUpdate
// ----------------------------------------------------------------------------
void ServerThread::handleMailUpdate(QByteArray data)
{
  msgs::MAIL_UPDATE msg;
  QDataStream stream(&data, QIODevice::ReadOnly);
  if (msg.decode(stream))
  {
    // The message can have multiple things to update, so check each one 
    // individually rather than a switch
    if (msg.update_type & msgs::MAIL_UPDATE::Read)
    {
      // TODO
    }
  }
  else
  {
    msgs::BAD bad("Failed to process MailUpdate message");
    send(&bad);
  }
}

// ----------------------------------------------------------------------------
// readSocket
// ----------------------------------------------------------------------------
void ServerThread::readSocket()
{
  // If blockSize is 0, we're starting a new message
  if (blockSize == 0)
  {
    // Make sure we have at least enough data to read the block size
    quint64 bytesAvail = socket->bytesAvailable();
    if (socket->bytesAvailable() < (int)sizeof(quint32)) {
      return;
    }

    // Read the block size (message size)
    QDataStream stream(socket);
    stream.setVersion(QDataStream::Qt_4_0);
    stream >> blockSize;
  }

  // Keep returning until we've received the entire message
  if (blockSize == 0 || socket->bytesAvailable() < blockSize) {
    return;
  }

  // Read the message
  QByteArray data = socket->read(blockSize);
  QDataStream stream(&data, QIODevice::ReadOnly);
  blockSize = 0;

  // Create a default message to read the message type
  msgs::MSG msg;
  if (msg.decode(stream))
  {
    // TEMP
    qDebug() << "DEBUG: Received message type " << msgs::typeToString(msg.type);
    // TEMP

    // The client must be successfully logged in for the server to accept 
    // any messages other than Login.  Handle that here for consistency.
    if (!isLoggedIn() && msg.type != msgs::Login)
    {
      msgs::BAD bad;
      bad.err = "No user logged in, server cannot accept incoming message.";
      send(&bad);
    }
    // It's also invalid to receive another Login message if the user is 
    // already logged in
    else if (isLoggedIn() && msg.type == msgs::Login)
    {
      // Send BAD message back to the client
      msgs::BAD bad;
      bad.err = "A user is already logged in on this connection, cannot login again.";
      send(&bad);
    }
    else
    {
      // Handle the incoming message
      switch (msg.type)
      {
      case msgs::Invalid: qDebug() << "Received \"Invalid\" Message";
      case msgs::Login: handleLogin(data); break;
      case msgs::ChangePassword: handleChangePassword(data); break;
      case msgs::Mail: handleMail(data); break;
      case msgs::MailDelete: handleMailDelete(data); break;
      case msgs::MailRequest: handleMailRequest(data); break;
      case msgs::MailUpdate: handleMailUpdate(data); break;
      default:
        qDebug() << "Currently not handling message type " << msgs::typeToString(msg.type);
      }
    }
  }
}

// ----------------------------------------------------------------------------
// clientDisconnected
// ----------------------------------------------------------------------------
void ServerThread::clientDisconnected()
{
  // Kill the thread
  if (!currentEmail.isEmpty()) {
    qDebug() << currentEmail << " has disconnected.";
  } else {
    qDebug() << "Client disconnected.";
  }
  exit(0);
}

// ----------------------------------------------------------------------------
// forwardMail
// ----------------------------------------------------------------------------
void ServerThread::forwardMail(msgs::MAIL* mail, QStringList recipients)
{
  // Confirm that this Mail is meant for this user
  if (recipients.contains(currentEmail) || recipients.contains(currentUser)) {
    qDebug() << "DEBUG: Forwarding mail to " << currentUser;
    send(mail);
  }
}
