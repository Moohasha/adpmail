#include <time.h>
#include <QtSql>
#include <QtCore/QDateTime>
#include <QtCore/QDebug>
#include "ServerDataBaseHelper.h"

namespace sdbh
{
  // --------------------------------------------------------------------------
  // attemptLogin
  // --------------------------------------------------------------------------
  bool attemptLogin(QString user, QByteArray password)
  {
    bool retval = false;

    // Do a query on the database to make sure that this user exists and that
    // the correct password was provided.
    QSqlQuery query;
    query.prepare("SELECT password FROM User WHERE email=?");
    query.bindValue(0, user);
    if (query.exec() && query.next())
    {
      QString dbpwd = query.value(0).toString();
      if (dbpwd == password)
      {
        // Good username and password.
        retval = true;
      }
    }
    
    return retval;
  }

  // --------------------------------------------------------------------------
  // changePassword
  // --------------------------------------------------------------------------
  bool changePassword(QString user, QByteArray password, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Update the specified user's password
    query.prepare("UPDATE User SET password=? WHERE email=?");
    query.bindValue(0, password);
    query.bindValue(1, user);
    retval = query.exec();
    if (!retval) {
      err = query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // saveMail
  // --------------------------------------------------------------------------
  bool saveMail(msgs::MAIL& mail, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Build query to store mail in database
    int i = 0;
    query.prepare("INSERT INTO Mail (category, to_emails, cc_emails, from_email, subject, sent_time, body) VALUES (?, ?, ?, ?, ?, ?, ?)");
    query.bindValue(i++, mail.category);
    query.bindValue(i++, mail.to.join(";"));
    query.bindValue(i++, mail.cc.join(";"));
    query.bindValue(i++, mail.from);
    query.bindValue(i++, mail.subject);
    query.bindValue(i++, mail.sent_time);
    query.bindValue(i++, mail.body);
    if (query.exec())
    {
      // Get the auto-generated UID from this new row
      mail.uid = query.lastInsertId().toUInt();

      // Save attachments
      QMap<QString, QByteArray>::const_iterator citr = mail.attachments.begin();
      for (; citr != mail.attachments.end(); ++citr)
      {
        QString filename = citr.key();

        // Generate random uuid
        QUuid att_uid = QUuid::createUuid();

        // Write attachment to file
        QFile file(ATTACHMENTS_PATH + att_uid.toString());
        if (file.open(QFile::WriteOnly))
        {
          file.write(citr.value());
          file.close();
          quint32 size = (quint32)file.size();

          // Write to Attachment table
          query.prepare("INSERT INTO Attachment (uid, filename, size) VALUES (?, ?, ?)");
          query.bindValue(0, att_uid.toString());
          query.bindValue(1, filename);
          query.bindValue(2, size);
          if (query.exec() == false) {
            qDebug() << "Failed to write attachment to database: " << query.lastError();
            file.remove();
            continue;
          }

          // Write to MailAtt table
          query.prepare("INSERT INTO MailAtt (att_id, mail_id) VALUES (?, ?)");
          query.bindValue(0, att_uid.toString());
          query.bindValue(1, mail.uid);
          if (query.exec() == false) {
            qDebug() << "Failed to associate attachment to mail: " << query.lastError();
          }
        }
      }

      retval = true;
    }
    else
    {
      err = query.lastError().text(); 
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // addMailToInbox
  // --------------------------------------------------------------------------
  bool addMailToInbox(msgs::MAIL& mail, QString recipient, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    query.prepare("INSERT INTO Inbox (user_id, mail_id, has_read) VALUES (?, ?, ?)");
    query.bindValue(0, recipient);
    query.bindValue(1, mail.uid);
    query.bindValue(2, mail.has_read); // always initially false
    retval = query.exec();
    err = query.lastError().text();

    return retval;
  }
  
  // --------------------------------------------------------------------------
  // deleteMail
  // --------------------------------------------------------------------------
  bool deleteMail(QList<unsigned int> uids, const QString& user, QString& err)
  {
    bool failed = false;

    QSqlQuery query;

    foreach (unsigned int uid, uids)
    {
      // Delete the mail reference from the Inbox table for this user
      query.prepare("DELETE FROM Inbox WHERE mail_id=? AND user_id=?");
      query.bindValue(0, uid);
      query.bindValue(1, user);
      if (query.exec())
      {
        // See if there are any more references to this email for any other
        // users.
        query.prepare("SELECT * FROM Inbox WHERE mail_id=?");
        query.bindValue(0, uid);
        if (query.exec() && !query.next())
        {
          // No more references to this mail, so delete it.
          query.prepare("DELETE FROM Mail WHERE uid=?");
          query.bindValue(0, uid);
          query.exec();

          // Also look up file attachments associated with the mail so they can
          // be deleted
          query.prepare("SELECT att_id FROM MailAtt WHERE mail_id=?");
          query.bindValue(0, uid);
          if (query.exec())
          {
            while (query.next())
            {
              QString att_uid = query.value(0).toString();

              // Remove file
              QFile::remove(ATTACHMENTS_PATH + att_uid);

              // Remove from Attachments table
              QSqlQuery query2;
              query2.prepare("DELETE FROM Attachment WHERE uid=?");
              query2.bindValue(0, att_uid);
              if (query2.exec() == false) {
                qDebug() << "Failed to delete attachment " << att_uid
                         << " from database: " << query2.lastError();
              }
            }
          }
          else
          {
            // Note: Simply returning 0 rows will not cause exec() to return
            // false, so this will truely only display errors.
            qDebug() << "Failed to lookup mail attachments: "
                     << query.lastError();
          }

          // Now remove from MailAtt table
          query.prepare("DELETE FROM MailAtt WHERE mail_id=?");
          query.bindValue(0, uid);
          if (query.exec() == false) {
            qDebug() << "Failed to delete mail attachments from database: "
                     << query.lastError();
          }
        }
      }
      else
      {
        err = query.lastError().text();
        failed = true;
      }
    }

    return !failed;
  }

  // --------------------------------------------------------------------------
  // checkForNewMail
  // --------------------------------------------------------------------------
  bool checkForNewMail(QString user, QList<unsigned int> existingUids, QList<unsigned int>& newUids, QString& err)
  {
    bool retval = false;

    QSqlQuery query;
    
    // Select all Mail uids from the Inbox for the given user
    query.prepare("SELECT mail_id FROM Inbox WHERE user_id=?");
    query.bindValue(0, user);
    if (query.exec())
    {
      while (query.next())
      {
        unsigned int uid = query.value(0).toUInt();

        // If this uid is not in the list of existing uids, add it to the list of new uids
        if (!existingUids.contains(uid)) {
          newUids << uid;
        }
      }
      retval = true;
    }

    return retval;
  }
  
  // --------------------------------------------------------------------------
  // getMail
  // --------------------------------------------------------------------------
  bool getMail(unsigned int uid, msgs::MAIL& mail, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Select all of the columns from the Mail table for the specified mail uid
    query.prepare("SELECT category, to_emails, cc_emails, from_email, subject, sent_time, body FROM Mail WHERE uid=?");
    query.bindValue(0, uid);
    if (query.exec())
    {
      if (query.next())
      {
        int i = 0;
        mail.uid = uid;
        mail.category   = query.value(i++).toInt();
        mail.to         = query.value(i++).toString().split(";");
        mail.cc         = query.value(i++).toString().split(";");
        mail.from       = query.value(i++).toString();
        mail.subject    = query.value(i++).toString();
        mail.sent_time  = query.value(i++).toDateTime();
        mail.body       = query.value(i++).toString();

        // Also do query on Inbox to get user-specific properties
        query.prepare("SELECT has_read FROM Inbox WHERE mail_id=?");
        query.bindValue(0, uid);
        if (query.exec())
        {
          if (query.next())
          {
            i = 0;
            mail.has_read = query.value(i++).toBool();
            retval = true;
          }
          else
          {
            err = "Could not find mail with uid " + QString::number(uid) + " in user's Inbox.";
          }
        }
        else
        {
          err = "Could not execute SQL command: " + query.lastError().text();
        }
      }
      else
      {
        err = "No mail found with uid " + QString::number(uid);
      }
    }
    else
    {
      err = "Could not execute SQL command: " + query.lastError().text();
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getUserName
  // --------------------------------------------------------------------------
  bool getUserName(QString email, QString& user, QString& err)
  {
    bool retval = false;

    QSqlQuery query;

    // Check the user name for a @ first to see if it's an email address
    if (email.contains('@'))
    {
      // Query for the email address
      query.prepare("SELECT l_name, f_name, m_name FROM User WHERE email=?");
      query.bindValue(0, email);
      if (query.exec())
      {
        if (query.next())
        {
          // Concatenate the first and last names
          user = QString("%1, %2").arg(query.value(0).toString()).arg(query.value(1).toString());

          // If there's a middle name, append it too
          QString m_name = query.value(2).toString();
          if (!m_name.isEmpty()) {
            user += " " + m_name;
          }
          retval = true;
        }
        else
        {
          err = "User " + email + " does not exist.";
        }
      }
      else
      {
        err = query.lastError().text();
      }
    }
    else
    {
      err = email + " is not a valid email address.";
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getUserEmail
  // --------------------------------------------------------------------------
  bool getUserEmail(QString user, QString& email, QString& err)
  {
    bool retval = false;

    QStringList nameParts = user.split(QRegExp("[, ]"), QString::SkipEmptyParts);
    if (nameParts.count() >= 2)
    {
      QString l_name = nameParts.at(0);
      QString f_name = nameParts.at(1);
      QString m_name = QString::null;
      
      if (nameParts.count() == 3) {
        m_name = nameParts.at(2);
      }

      // Query for the email address.  Because the middle name is optional, it 
      // is not included in the query.
      QSqlQuery query;
      query.prepare("SELECT email, m_name FROM User WHERE l_name=? AND f_name=?");
      query.bindValue(0, l_name);
      query.bindValue(1, f_name);
      if (query.exec())
      {
        // Because the middle name is not included in the query, multiple rows
        // could be returned.  Search all of them until a full name match is 
        // made.
        while (query.next())
        {
          QString dbm_name = query.value(1).toString();
          if (m_name == dbm_name)
          {
            email = query.value(0).toString();
            retval = true;
            break;
          }
        }

        // See if a match was found
        if (!retval)
        {
          err = "User " + user + " does not exist.";
        }
      }
      else
      {
        err = query.lastError().text();
      }
    }
    else
    {
      err = "Invalid user name: " + user;
    }

    return retval;
  }

  // --------------------------------------------------------------------------
  // getClients
  // --------------------------------------------------------------------------
  bool getClients(msgs::CLIENTS& clients, QString& err)
  {
    bool retval = false;

    // Query for all users from the User table
    QSqlQuery query;
    query.prepare("SELECT email, l_name, f_name, m_name FROM User");
    if (query.exec())
    {
      retval = true;

      while (query.next())
      {
        // Get email
        QString email = query.value(0).toString();
        if (!email.isEmpty())
        {
          // Get name
          QString name = QString("%1, %2")
            .arg(query.value(1).toString())  // l_name
            .arg(query.value(2).toString()); // f_name

          QString mi = query.value(3).toString();
          if (!mi.isEmpty()) {
            name += " " + mi;
          }

          // Add to message
          clients.names.insert(email, name);
        }
      }
    }
    else
    {
      err = query.lastError().text();
    }

    return retval;
  }
} // end namespace 
