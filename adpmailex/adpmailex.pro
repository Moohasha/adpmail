#-------------------------------------------------
#
# Project created by QtCreator 2013-03-26T20:23:23
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

TARGET = adpmailex
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    ServerThread.cpp \
    Server.cpp \
    ServerDataBaseHelper.cpp \
    ../common.cpp

HEADERS += \
    ../common.h \
    Server.h \
    ServerThread.h \
    ServerDataBaseHelper.h \

OTHER_FILES += \
    init_db.sql
