#ifndef SERVERDATABASEHELPER_H
#define SERVERDATABASEHELPER_H
#include <QtCore/QString>
#include "../common.h"

// Server Database Helper (sdbh) methods
namespace sdbh
{
  // Check the User table for the specified user and verify that the password
  // is correct.
  bool attemptLogin(QString user, QByteArray password);

  // Change the specified user's password.
  bool changePassword(QString user, QByteArray password, QString& err);

  // Save the Mail message to the database.
  // Note: The uid of the Mail message may change based on the result of 
  // inserting it into the database.
  bool saveMail(msgs::MAIL& mail, QString& err);

  // Add a reference to the Mail message to the specified recipient
  bool addMailToInbox(msgs::MAIL& mail, QString recipient, QString& err);

  // Delete all specified mail for the specified user.  If no other users 
  // reference a Mail message, it will be deleted completely.
  bool deleteMail(QList<unsigned int> uids, const QString& user, QString& err);

  // Lookup all Mail uids that are not included in the existingUids list.
  bool checkForNewMail(QString user, QList<unsigned int> existingUids, QList<unsigned int>& newUids, QString& err);

  // Lookup the specified Mail.
  bool getMail(unsigned int uid, msgs::MAIL& mail, QString& err);

  // Check the email address passed in and return the user name associated with
  // it
  bool getUserName(QString email, QString& user, QString& err);

  // Check the user name passed in and return the email address associated with
  // it.  The user name should be of the form "last, first <middle>" (middle 
  // name is optional).
  bool getUserEmail(QString user, QString& email, QString& err);

  // Get all clients' info.
  bool getClients(msgs::CLIENTS& clients, QString& err);
} // end namespace

#endif // SERVERDATABASEHELPER_H