#include <QCoreApplication>
#include "Server.h"

int main(int argc, char *argv[])
{  
  QCoreApplication a(argc, argv);
  Server server;
  if (server.init()) // Use IMAP port
  {
    return a.exec();
  }
  return -1;
}
