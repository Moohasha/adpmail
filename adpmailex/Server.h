#ifndef SERVER_H
#define SERVER_H

#include <QtCore/QTextStream>
#include <QtCore/QThread>
#include <QtNetwork/QTcpServer>

class Server : QTcpServer
{
public:
  explicit Server();
  ~Server();

  bool init();

private:
  bool initDB();
  bool initServer();
  void incomingConnection(int);

private:
  QTextStream cout;
};

#endif // SERVER_H
