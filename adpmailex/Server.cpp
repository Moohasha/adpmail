#include <QtSql>
#include "Server.h"
#include "ServerThread.h"
#include "../common.h"

// ----------------------------------------------------------------------------
// ctor
// ----------------------------------------------------------------------------
Server::Server()
{
}

// ----------------------------------------------------------------------------
// dtor
// ----------------------------------------------------------------------------
Server::~Server()
{
}

// ----------------------------------------------------------------------------
// init
// ----------------------------------------------------------------------------
bool Server::init()
{
  bool retval = false;
  if (initDB()) {
    retval = initServer();
  }
  return retval;
} 

// ----------------------------------------------------------------------------
// initDB
// ----------------------------------------------------------------------------
bool Server::initDB()
{
  bool retval = false;

  QString version = QString("Server version %1.%2").arg(SERVER_VERSION_MAJOR).arg(SERVER_VERSION_MINOR);
  qDebug() << version;
  qDebug() << "Initializing database...";
  
  // Connect to the mail database
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("mail.sqlite");
  if (db.open())
  {
    // Look for the init_db file.  If it exists, load the scripts from it and
    // run them
    QFile file("init_db.sql");
    if (file.open(QFile::ReadOnly))
    {
      qDebug() << "Running init_db script...";
      QString scriptFile(file.readAll());
      QStringList scripts(scriptFile.split(";", QString::SkipEmptyParts));
      foreach (QString script, scripts)
      {
        // Make sure the "script" isn't just whitespace
        script = script.trimmed();
        if (!script.isEmpty())
        {
          QSqlQuery query;
          if (!query.exec(script)) {
            qDebug() << "Error initializing server database: " << query.lastError().text();
          }
        }
      }

      // Make sure the attachments path exists
      QDir dir;
      if (!dir.exists(ATTACHMENTS_PATH)) {
        dir.mkpath(ATTACHMENTS_PATH);
      }
//      // Now rename the file so it doesn't get loaded again, but doesn't get
//      // removed either.
//      file.close();
//      file.rename("init_db.sql.old");
    }
    retval = true;
  } else {
    qDebug() << "Failed to open database.";
  }

  return retval;
}

// ----------------------------------------------------------------------------
// initServer
// ----------------------------------------------------------------------------
bool Server::initServer()
{
  // Initialize the TCP server
  qDebug() << "Initializing server...";
  bool retval = listen(QHostAddress::Any, SERVER_PORT);
  if (retval) {
    qDebug() << "Server running on port " << serverPort();
  } else {
    qDebug() << "Failed to start server.";
  }
  return retval;
}

// ----------------------------------------------------------------------------
// incomingConnection
// ----------------------------------------------------------------------------
void Server::incomingConnection(int sd)
{
  qDebug() << "Received new connection.";

  // New connection.  Create a separate thread to handle it.
  ServerThread *thread = new ServerThread(sd);
  connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()), Qt::QueuedConnection);
  thread->start();
  thread->moveToThread(thread);
}
