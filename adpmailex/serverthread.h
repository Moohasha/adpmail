#ifndef SERVERTHREAD_H
#define SERVERTHREAD_H

#include <QtCore/QThread>
#include <QtCore/QSet>
#include <QtCore/QMutex>

class QTcpSocket;
namespace msgs {struct MSG;}
namespace msgs {struct MAIL;}

class ServerThread : public QThread
{
  Q_OBJECT

signals:
  void error(int socketError);

public:
  explicit ServerThread(int sd);
  ~ServerThread();

private:
  void send(msgs::MSG* msg);
  void run();
  void handleLogin(QByteArray data);
  void handleChangePassword(QByteArray data);
  void handleMail(QByteArray data);
  void handleMailDelete(QByteArray data);
  void handleMailRequest(QByteArray data);
  void handleMailUpdate(QByteArray data);
  void forwardMail(msgs::MAIL* mail, QStringList recipients);
  QString getCurrentUser() const {return currentUser;}
  bool isLoggedIn() const {return !currentUser.isEmpty();}

private slots:
  void readSocket();
  void clientDisconnected();

private:
  static QSet<ServerThread*> msThreads;
  static QMutex msMutex; // for the set of threads

  int socketDescriptor;
  QString currentEmail;
  QString currentUser;
  QTcpSocket* socket;
  quint32 blockSize;
};

#endif // SERVERTHREAD_H
