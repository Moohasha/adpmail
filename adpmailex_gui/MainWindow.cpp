#include <QtCore/QDebug>
#include <QtCore/QFile>
#include <QtCore/QDir>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QMessageBox>
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "ui_StatusWidget.h"

// ----------------------------------------------------------------------------
// ctor
// ----------------------------------------------------------------------------
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow),
  uiStatus(new Ui::StatusWidget)
{
  ui->setupUi(this);
  connect(ui->sqlEdit->lineEdit(), SIGNAL(returnPressed()), ui->sqlExecButton, SLOT(click()));

  QWidget* widget = new QWidget;
  uiStatus->setupUi(widget);
  statusBar()->addPermanentWidget(widget);
  uiStatus->light->setPixmap(QPixmap(":/images/status_red"));
  uiStatus->status->setText("Database Connection:");

  // Establish the connection to the database
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  db.setDatabaseName("mail.sqlite");
  if (db.open())
  {
    uiStatus->light->setPixmap(QPixmap(":/images/status_green"));
    refreshClicked();
  }

  loadFavorites();
}

// ----------------------------------------------------------------------------
// dtor
// ----------------------------------------------------------------------------
MainWindow::~MainWindow()
{
  delete ui;
}

// ----------------------------------------------------------------------------
// loadFavorites
// ----------------------------------------------------------------------------
void MainWindow::loadFavorites()
{
  QDir dir;
  if (dir.cd("Favorites"))
  {
    QFileInfoList files = dir.entryInfoList(QStringList() << "*.sql");
    foreach (QFileInfo info, files)
    {
      qDebug() << "Loading SQL favorite: " << info.filePath();
      QFile file(info.filePath());
      if (file.open(QFile::ReadOnly)) {
        sqlFavorites.insert(info.fileName(), file.readAll().trimmed());
      }
    }
  }

  if (sqlFavorites.empty()) {
    ui->favoritesLabel->setText("<i>No Favorites</i>");
  }
}

// ----------------------------------------------------------------------------
// userSelected
// ----------------------------------------------------------------------------
void MainWindow::userSelected(QListWidgetItem *item)
{
  QStringList names = item->text().split(QRegExp("[, ]"), QString::SkipEmptyParts);
  if (names.count() >= 2)
  {
    QString lname = names.at(0);
    QString fname = names.at(1);
    QString mname;

    // If there's more than just the first and last name, join the remainder into
    // a single string for the middle name.  This allows for multiple middle
    // initials.
    if (names.count()>2)
    {
      names.removeFirst();
      names.removeFirst();
      mname = names.join(" ");
    }

    QString queryStr = QString("SELECT email, password, f_name, m_name, l_name FROM User "
                               "WHERE f_name=\'%1\' AND m_name=\'%2\' AND l_name=\'%3\'")
        .arg(fname)
        .arg(mname)
        .arg(lname);

    QSqlQuery query;
    if (query.exec(queryStr))
    {
      query.next();
      ui->emailEdit->setText(query.value(0).toString());
      ui->passwordEdit1->setText(query.value(1).toString());
      ui->passwordEdit2->setText(query.value(1).toString());
      ui->fnameEdit->setText(query.value(2).toString());
      ui->mnameEdit->setText(query.value(3).toString());
      ui->lnameEdit->setText(query.value(4).toString());
    }
    else
    {
      statusBar()->showMessage(query.lastError().text());
    }
  }
}

// ----------------------------------------------------------------------------
// addEditClicked
// ----------------------------------------------------------------------------
void MainWindow::addEditClicked()
{
  QSqlQuery query;
  QString queryStr;

  // Get values from ui
  QString fname = ui->fnameEdit->text();
  QString mname = ui->mnameEdit->text();
  QString lname = ui->lnameEdit->text();
  QString email = ui->emailEdit->text();
  QString pwd1  = ui->passwordEdit1->text();
  QString pwd2  = ui->passwordEdit2->text();

  // Verify first and last name are filled out
  if (fname.isEmpty() || lname.isEmpty())
  {
    QMessageBox::critical(this, "Add/Update User",
                          "First and last name are required");
    return;
  }

  // Verify email edit filled out
  if (pwd1.isEmpty() || pwd2.isEmpty())
  {
    QMessageBox::critical(this, "Add/Update User",
                          "Email required");
    return;
  }

  // Verify both password edits filled out
  if (pwd1.isEmpty() || pwd2.isEmpty())
  {
    QMessageBox::critical(this, "Add/Update User",
                          "Password required");
    return;
  }

  // Verify both password edits match
  if (pwd1.compare(pwd2, Qt::CaseInsensitive) != 0)
  {
    QMessageBox::critical(this, "Add/Update User",
                          "Passwords do not match");
    return;
  }

  // Verify that name is unique using SQL query
  queryStr = QString("SELECT email FROM User WHERE f_name=\'%1\' AND m_name=\'%2\' AND l_name='%3\'")
      .arg(fname)
      .arg(mname)
      .arg(lname);
  if (query.exec(queryStr) && query.next())
  {
    // Name sure the email address matches what is on the form.  Otherwise, we
    // can't create a new user with the same name as some body else.
    QString record_email = query.value(0).toString();
    if (record_email != email)
    {
      QMessageBox::critical(this, "Add/Update User", "Another user already exists with this name, "
                            "cannot create new user.");
      return;
    }
  }

  // See if a User already exists with this email.  If so, prompt to update the
  // existing user, because we can't create a new one with the same email.
  queryStr = QString("SELECT email FROM User WHERE email=\'%1\'").arg(email);
  if (query.exec(queryStr) && query.next())
  {
    int r = QMessageBox::question(this, "Update User",
                          "A user already exists with this email address.  Do you wish to update that user?",
                          QMessageBox::Yes, QMessageBox::No);
    if (r == QMessageBox::Yes)
    {
      // Update the selected User with new details
      queryStr = QString("UPDATE User SET password=\'%2\', f_name=\'%3\', m_name=\'%4\', l_name=\'%5\' WHERE email=\'%1\'")
          .arg(email)
          .arg(pwd1)
          .arg(fname)
          .arg(mname)
          .arg(lname);

      // Perform the query
      if (query.exec(queryStr)) {
        QMessageBox::information(this, "Add/Update User", "Update complete!");
        refreshClicked();
      } else {
        statusBar()->showMessage(query.lastError().text(), 10000);
      }
    }
  }
  else
  {
    // Email is unique, so insert a new record into the User table.
    QString queryStr = QString("INSERT INTO User (email, password, f_name, m_name, l_name) "
                               "VALUES (\'%1\', \'%2\', \'%3\', \'%4\', \'%5\')")
        .arg(email)
        .arg(pwd1)
        .arg(fname)
        .arg(mname)
        .arg(lname);

    // Perform the query
    if (query.exec(queryStr)) {
      QMessageBox::information(this, "Add/Update User", "New user added!");
      refreshClicked();
    } else {
      statusBar()->showMessage(query.lastError().text(), 10000);
    }
  }
}

// ----------------------------------------------------------------------------
// refreshClicked
// ----------------------------------------------------------------------------
void MainWindow::refreshClicked()
{
  QString USER_QUERY("SELECT f_name, m_name, l_name FROM User");

  // Clear the current list
  ui->userList->clear();

  QSqlQuery query;
  if (query.exec(USER_QUERY))
  {
    // Get the names from the query
    QStringList names;
    while (query.next())
    {
      QString fname = query.value(0).toString();
      QString mname = query.value(1).toString();
      QString lname = query.value(2).toString();
      names << QString("%1, %2 %3").arg(lname).arg(fname).arg(mname);
    }

    // Add all names to the list
    names.sort();
    ui->userList->addItems(names);
  }
}

// ----------------------------------------------------------------------------
// deleteClicked
// ----------------------------------------------------------------------------
void MainWindow::deleteClicked()
{
  int r = QMessageBox::question(this, "Delete User", "Are you sure you want to delete this user?",
                                QMessageBox::Yes, QMessageBox::No);
  if (r == QMessageBox::Yes)
  {
    QSqlQuery query;
    QString queryStr;
    QString email = ui->emailEdit->text();

    queryStr = QString("DELETE FROM User WHERE email=\'%1\'").arg(email);
    if (query.exec(queryStr)) {
      QMessageBox::information(this, "Delete User", "User Deleted!");
      refreshClicked();
    } else {
      statusBar()->showMessage(query.lastError().text());
    }

    // TODO: Delete mail from Inbox, remove user from UserGroup, etc.
  }
}

// ----------------------------------------------------------------------------
// execQuery
// ----------------------------------------------------------------------------
void MainWindow::execQuery()
{
  // Clear the table.
  ui->sqlTable->clear();
  ui->sqlTable->setRowCount(0);
  ui->sqlTable->setColumnCount(0);

  // Perform a new query
  QString text = ui->sqlEdit->currentText().trimmed();
  QSqlQuery query;
  if (query.exec(text))
  {
    statusBar()->showMessage("Query successful");
    // Set the table column headers
    QSqlRecord record = query.record();
    ui->sqlTable->setColumnCount(record.count());
    QStringList labels;
    for (int i = 0; i < record.count(); ++i) {
      labels << record.fieldName(i);
    }
    ui->sqlTable->setHorizontalHeaderLabels(labels);

    // Process the rows
    int row = 0;
    while (query.next())
    {
      ui->sqlTable->setRowCount(ui->sqlTable->rowCount()+1);
      for (int i = 0; i < record.count(); ++i) {
        QString text = query.value(i).toString();
        ui->sqlTable->setItem(row, i, new QTableWidgetItem(text));
      }
      row++;
    }
  }
  else
  {
    statusBar()->showMessage("Query failed: " + query.lastError().text());
  }

  // Insert this query into the combo box
  ui->sqlEdit->insertItem(0, text);
}

// ----------------------------------------------------------------------------
// favoritesClicked
// ----------------------------------------------------------------------------
void MainWindow::favoritesClicked()
{
  if (!sqlFavorites.empty())
  {
    QMenu menu;
    foreach (QString name, sqlFavorites.keys()) {
      menu.addAction(name);
    }

    if (QAction* action = menu.exec(QCursor::pos()))
    {
      QString queryStr = sqlFavorites.value(action->text());
      ui->sqlEdit->lineEdit()->setText(queryStr);
    }
  }
}
