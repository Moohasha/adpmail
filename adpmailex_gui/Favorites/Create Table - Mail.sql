CREATE TABLE Mail
(
  uid INTEGER PRIMARY KEY AUTOINCREMENT,
  category INT NOT NULL,
  to_emails TEXT NOT NULL,
  cc_emails TEXT,
  from_email CHAR(128) NOT NULL,
  subject CHAR(128) NOT NULL,
  sent_time DATETIME NOT NULL,
  body TEXT
);
