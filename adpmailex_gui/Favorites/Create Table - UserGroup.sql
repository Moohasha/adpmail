CREATE TABLE UserGroup
(
  group_id INT NOT NULL,
  user_id CHAR(128) NOT NULL,
  FOREIGN KEY (group_id) REFERENCES MailGroup(uid),
  FOREIGN KEY (user_id) REFERENCES User(email)
);
