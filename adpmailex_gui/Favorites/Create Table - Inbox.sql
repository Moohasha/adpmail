CREATE TABLE Inbox
(
  user_id CHAR(128) NOT NULL,
  mail_id INT NOT NULL,
  FOREIGN KEY (user_id) REFERENCES User(email),
  FOREIGN KEY (mail_id) REFERENCES Mail(uid)
);
