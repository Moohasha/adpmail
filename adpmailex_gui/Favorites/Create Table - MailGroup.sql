CREATE TABLE MailGroup
(
  uid INT NOT NULL UNIQUE,
  name CHAR(128) NOT NULL,
  desc CHAR(255),
  PRIMARY KEY (uid)
);
