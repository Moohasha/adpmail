CREATE TABLE User
(
  email CHAR(128) NOT NULL UNIQUE,
  password CHAR(128) NOT NULL,
  f_name CHAR(32),
  m_name CHAR(8),
  l_name CHAR(32),
  PRIMARY KEY (email)
);
