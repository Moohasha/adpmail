#-------------------------------------------------
#
# Project created by QtCreator 2013-04-10T21:32:03
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = adpmailex_gui
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp\
        ../common.cpp

HEADERS  += MainWindow.h\
        ../common.h

FORMS    += MainWindow.ui\
        ../StatusWidget.ui

RESOURCES += \
    ../resources/resources.qrc
