#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QMap>

namespace Ui {
class MainWindow;
class StatusWidget;
}
class QListWidgetItem;

class MainWindow : public QMainWindow
{
  Q_OBJECT
  
public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();
  
private:
  void loadFavorites();

private slots:
  void userSelected(QListWidgetItem*);
  void addEditClicked();
  void refreshClicked();
  void deleteClicked();
  void execQuery();
  void favoritesClicked();

private:
  Ui::MainWindow *ui;
  Ui::StatusWidget *uiStatus;
  QMap<QString, QString> sqlFavorites;
};

#endif // MAINWINDOW_H
