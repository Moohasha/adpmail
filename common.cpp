#include <QtCore/QDataStream>
#include "common.h"

#define ENUM_CASE(x) case x: strType = #x; break;

namespace msgs
{
  QString typeToString(Type type)
  {
    QString strType;
    switch (type)
    {
      ENUM_CASE(Bad)
      ENUM_CASE(ChangePassword)
      ENUM_CASE(Clients)
      ENUM_CASE(Login)
      ENUM_CASE(Mail)
      ENUM_CASE(MailDelete)
      ENUM_CASE(MailRequest)
      ENUM_CASE(MailUpdate)
      ENUM_CASE(Ok)

      default:
        strType = "Invalid";
    }
    return strType;
  }

  // --- MSG --------------------------------------------------------------------

  void MSG::encode(QDataStream& stream) const
  {
    stream << (quint16)type;
  }

  bool MSG::decode(QDataStream& stream)
  {
    bool retval = false;

    // Read the type from the data
    quint16 temp;
    stream >> temp;
    type = (Type)temp;
    retval = true;

    return retval;
  }




  // --- LOGIN ----------------------------------------------------------------

  void LOGIN::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << email.toLower();
    stream << password; // TODO: encrpt
  }

  bool LOGIN::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == Login)
    {
      stream >> email;
      stream >> password; // TODO: encrpt
      retval = true;
    }

    return retval;
  }
  



  // --- CHANGE PASSWORD ------------------------------------------------------

  void CHG_PWD::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << old_pwd; // TODO: encrpt
    stream << new_pwd; // TODO: encrpt
  }

  bool CHG_PWD::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == ChangePassword)
    {
      stream >> old_pwd; // TODO: encrpt
      stream >> new_pwd; // TODO: encrpt
      retval = true;
    }

    return retval;
  }



  // --- MAIL DELETE ----------------------------------------------------------

  void MAIL_DEL::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << uids;
  }

  bool MAIL_DEL::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == MailDelete)
    {
      stream >> uids;
      retval = true;
    }

    return retval;
  }
  



  // --- MAIL REQUEST ---------------------------------------------------------

  void MAIL_REQ::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << uids;
  }

  bool MAIL_REQ::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == MailRequest)
    {
      stream >> uids;
      retval = true;
    }

    return retval;
  }





  // --- MAIL UPDATE ----------------------------------------------------------

  void MAIL_UPDATE::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << uid;
    stream << (quint16)update_type;
    stream << has_read;
  }

  bool MAIL_UPDATE::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == MailUpdate)
    {
      quint16 temp;
      stream >> uid;
      stream >> temp;
      stream >> has_read;
      update_type = (UpdateType)temp;
      retval = true;
    }

    return retval;
  }




  // --- MAIL -----------------------------------------------------------------

  void MAIL::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << uid;
    stream << category;
    stream << has_read;
    stream << from;
    stream << to;
    stream << cc;
    stream << subject;
    stream << sent_time;
    stream << body;
    stream << attachments;
  }

  bool MAIL::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == Mail)
    {
      stream >> uid;
      stream >> category;
      stream >> has_read;
      stream >> from;
      stream >> to;
      stream >> cc;
      stream >> subject;
      stream >> sent_time;
      stream >> body;
      stream >> attachments;
      retval = true;
    }

    return retval;
  }




  // --- BAD ------------------------------------------------------------------

  void BAD::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << err;
  }

  bool BAD::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == Bad)
    {
      stream >> err;
      retval = true;
    }

    return retval;
  }





  // --- CLIENTS --------------------------------------------------------------

  void CLIENTS::encode(QDataStream& stream) const
  {
    // Let the base class initialize the byte array
    MSG::encode(stream);
    stream << names;
  }

  bool CLIENTS::decode(QDataStream& stream)
  {
    bool retval = false;

    if (MSG::decode(stream) && type == Clients)
    {
      stream >> names;
      retval = true;
    }

    return retval;
  }
} // end namespace msgs
