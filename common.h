#ifndef COMMON_H
#define COMMON_H
#include <QtCore/QMap>
#include <QtCore/QStringList>
#include <QtCore/QString>
#include <QtCore/QDateTime>
#include <QtCore/QUuid>

static const quint16 SERVER_PORT = 19300;
static const quint32 MAX_ATTACHMENT_SIZE = 1024*1024*20; // 20 MB total
static const QString ATTACHMENTS_PATH("./files/");

// Client version: 1.1
static const unsigned short CLIENT_VERSION_MAJOR = 1;
static const unsigned short CLIENT_VERSION_MINOR = 2;
// Server version 1.1
static const unsigned short SERVER_VERSION_MAJOR = 1;
static const unsigned short SERVER_VERSION_MINOR = 2;
class QDataStream;

namespace msgs
{
  enum Type
  {
    Invalid,

    // Client messages
    Login,
    ChangePassword,
    MailDelete,
    MailRequest,
    MailUpdate,

    // Client and Server messages
    Mail,

    // Server messages
    Ok,
    Bad,
    Clients
  };

  QString typeToString(Type type);

  // Base class for all messages
  struct MSG
  {
    MSG() : type(Invalid) {}
    MSG(Type type) : type(type) {}
    virtual ~MSG() {}
    virtual void encode(QDataStream&) const;
    virtual bool decode(QDataStream&);
    Type type;
  };

  // === Client Messages ======================================================
  // Used to establish a user connection.  Can fail if the user name or
  // password are invalid.
  // Success Response: OK
  struct LOGIN : public MSG
  {
    LOGIN() : MSG(Login) {}
    QString email;
    QByteArray password;
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // Used to change the user's password.  The user must successfully login 
  // first using the existing password before it can be changed.
  struct CHG_PWD : public MSG
  {
    CHG_PWD() : MSG(ChangePassword) {}
    QByteArray old_pwd; // the old password.
    QByteArray new_pwd; // the new password.
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // Used to tell the server to delete the specified mail from the user's inbox.
  // Success Response: OK
  struct MAIL_DEL : public MSG
  {
    MAIL_DEL() : MSG(MailDelete) {}
    QList<unsigned int> uids;
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // Used to request Mail from the server that the client doesn't have.  The
  // message includes the uids of all Mail that the client DOES have, and the
  // server is expected to reply with any additional Mail that the client 
  // doesn't have.
  // Success Response: OK, Followed by 0..n MAIL
  struct MAIL_REQ : public MSG
  {
    MAIL_REQ() : MSG(MailRequest) {}
    QList<unsigned int> uids;
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // Notify the server that this Mail has been updated.  The update type is
  // specified within the message and determines what other properties in the
  // message are populated.
  struct MAIL_UPDATE : public MSG
  {
    enum UpdateType
    {
      None = 0,
      Read = 1
    };

    MAIL_UPDATE() : MSG(MailUpdate) {}
    unsigned int uid;
    UpdateType update_type;
    bool has_read;
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // ==========================================================================




  // === Client/Server Messages ===============================================
  // Used to send/receive mail.
  struct MAIL : public MSG
  {
    MAIL() : MSG(Mail), uid(0), category(0) {}
    unsigned int uid; // unique id of this MAIL
    unsigned int category;
    bool has_read;
    QStringList to;
    QStringList cc;
    QString from;
    QString subject;
    QDateTime sent_time;
    QString body;
    QMap<QString, QByteArray> attachments; // key = filename, value = file data
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };
  // ==========================================================================




  // === Server Messages ======================================================
  // Used to notify the client that whatever request was just made was
  // accepted.
  struct OK : public MSG
  {
    OK() : MSG(Ok) {}
  };

  // Used to notify the client that whatever request was just made could not be
  // completed.  Contains the appropriate error message.
  struct BAD : public MSG
  {
    BAD(QString error = QString()) : MSG(Bad), err(error) {}
    QString err; // error message
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // Sends a list of all client emails and user names so that the user can 
  // associate names to email addresses.  This allows the client to use auto
  // complete features.
  struct CLIENTS : public MSG
  {
    CLIENTS() : MSG(Clients) {}
    QMap<QString, QString> names; // key = email, value = user name (last, first mi)
    void encode(QDataStream&) const;
    bool decode(QDataStream&);
  };

  // ==========================================================================
} // end namespace msgs

#endif // COMMON_H
